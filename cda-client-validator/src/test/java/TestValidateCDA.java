import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.LoggingFilter;
import com.sun.jersey.api.json.JSONConfiguration;

import goimplement.it.CdaType;
import goimplement.it.Hl7JkiddoRhcloudCom_Service;
import goimplement.it.Hl7JkiddoRhcloudCom_Service.CDA;
import goimplement.it.ValidationResponse;

public class TestValidateCDA {

	public static void main(String[] args) throws MalformedURLException, IOException {

		ClientConfig config = new DefaultClientConfig();
		config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		Client client = Client.create(config);
		client.addFilter(new LoggingFilter());
		CDA cdaValidationClient = Hl7JkiddoRhcloudCom_Service.cDA(client);
		ValidationResponse result = null;
		result = cdaValidationClient.validateCdaType(CdaType.PHMR.name()).postXmlAsJson("",
				ValidationResponse.class);
		System.out.println(result);

		final String document = Resources.toString(new URL("file:src/main/resources/QRDOC.sample.xml"), Charsets.UTF_8);
		
		result = cdaValidationClient.validateCdaType(CdaType.PHMR.name()).postXmlAsJson(document,
				ValidationResponse.class);
		System.out.println(result);
	}
}
