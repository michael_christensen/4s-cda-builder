
package goimplement.it;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for cdaType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="cdaType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PHMR"/>
 *     &lt;enumeration value="QFDD"/>
 *     &lt;enumeration value="QRDOC"/>
 *     &lt;enumeration value="NONE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "cdaType")
@XmlEnum
public enum CdaType {

    PHMR,
    QFDD,
    QRDOC,
    NONE;

    public String value() {
        return name();
    }

    public static CdaType fromValue(String v) {
        return valueOf(v);
    }

}
