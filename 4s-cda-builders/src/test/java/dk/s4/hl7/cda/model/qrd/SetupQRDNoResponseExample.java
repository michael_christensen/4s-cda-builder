package dk.s4.hl7.cda.model.qrd;

import java.util.Date;
import java.util.UUID;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.testutil.HelperMethods;
import dk.s4.hl7.cda.model.testutil.Setup;

/**
 * Helper methods to create qrd without responses
 * 
 * @author Frank Jacobsen, Systematic
 *
 */
public class SetupQRDNoResponseExample {

  public static QRDDocument defineAsCDA() {
    QRDDocument defineQRDDocumentHeaderInformation = defineQRDDocumentHeaderInformation();
    return defineQRDDocumentHeaderInformation;
  }

  /** Define a CDA for Numeric example */
  public static QRDDocument defineQRDDocumentHeaderInformation() {

    // Define the 'time'
    Date documentCreationTime = HelperMethods.makeDanishDateTime(2014, 0, 13, 10, 0, 0);

    // Setup Anders Andersen as authenticator
    PersonIdentity andersAndersen = new PersonIdentity.PersonBuilder("Andersen").addGivenName("Anders").build();

    // 1. Create a PHMR document as a "Green CDA", that is,
    // a data structure containing only the dynamic data
    // of a CDA.
    ID idHeader = new ID.IDBuilder()
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .setExtension(UUID.randomUUID().toString())
        .setRoot(MedCom.ROOT_OID)
        .build();
    QRDDocument qrdDocument = new QRDDocument(idHeader);
    qrdDocument.setLanguageCode("da-DK");
    qrdDocument.setTitle("KOL spørgeskema");

    // 1.1 Populate with time and version info
    qrdDocument.setDocumentVersion("2358344", 1);
    qrdDocument.setEffectiveTime(documentCreationTime);

    // 1.2 Populate the document with patient information
    Patient nancy = Setup.defineNancyAsFullPersonIdentity();
    qrdDocument.setPatient(nancy);

    // 1.3 Populate with Author, Custodian, and Authenticator
    // Setup Svendborg sygehus Hjertemedicinsk B as organization
    OrganizationIdentity custodianOrganization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("88878685")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .setAddress(Setup.defineHjerteMedicinskAfdAddress())
        .addTelecom(Use.WorkPlace, "tel", "65223344")
        .build();

    qrdDocument.setCustodian(custodianOrganization);

    OrganizationIdentity organization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("88878685")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .build();

    qrdDocument.setAuthor(new ParticipantBuilder()
        .setAddress(custodianOrganization.getAddress())
        .setSOR(custodianOrganization.getIdValue())
        .setTelecomList(custodianOrganization.getTelecomList())
        .setTime(documentCreationTime)
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(organization)
        .build());

    Date at1000onJan13 = HelperMethods.makeDanishDateTime(2014, 0, 13, 10, 0, 0);

    qrdDocument.setLegalAuthenticator(new ParticipantBuilder()
        .setAddress(custodianOrganization.getAddress())
        .setSOR(custodianOrganization.getIdValue())
        .setTelecomList(custodianOrganization.getTelecomList())
        .setTime(at1000onJan13)
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(organization)
        .build());

    // 1.4 Define the service period
    Date from = HelperMethods.makeDanishDateTime(2014, 0, 6, 8, 2, 0);
    Date to = HelperMethods.makeDanishDateTime(2014, 0, 10, 8, 15, 0);
    qrdDocument.setDocumentationTimeInterval(from, to);

    String text = "1. Heart failure affects different people in different ways. Some feel shortness of breath while others feel fatigue. Please<br/>"
        + "indicate how much you are limited by heart failure (shortness of breath or fatigue) in your ability to do the following<br/>"
        + "activities over the past 2 weeks.";

    Section<QRDResponse> qRDSection = new Section<QRDResponse>("Section Title", text);

    qrdDocument.addSection(qRDSection);

    return qrdDocument;
  }
}
