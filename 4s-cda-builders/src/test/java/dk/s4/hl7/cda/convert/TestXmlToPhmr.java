package dk.s4.hl7.cda.convert;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;

import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.Difference;
import org.custommonkey.xmlunit.DifferenceListener;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import dk.s4.hl7.cda.model.phmr.PHMRDocument;
import dk.s4.hl7.cda.model.testutil.HelperMethods;

public final class TestXmlToPhmr {
  private PHMRXmlCodec codec = new PHMRXmlCodec();
  private PHMRDocument cda;

  @Before
  public void setup() {
  }

  @Test
  public void testPHMRXmlConverterMedcomExample1() throws Exception {
    cda = SetupMedcomExample1.defineAsCDA();

    String expectedEncodedXml = null;
    PHMRDocument expectedDecoded = null;
    String expectedEncodedNew = null;
    try {
      expectedEncodedXml = encode(cda);
      byte ptext[] = expectedEncodedXml.getBytes();
      String value = new String(ptext, "UTF-8");
      expectedDecoded = decode(value);
      expectedEncodedNew = encode(expectedDecoded);
      compare(expectedEncodedXml, expectedEncodedNew);
    } catch (Exception e) {
      outXml(expectedEncodedXml, expectedEncodedNew, new StringBuffer("Kan ikke parse XML, det er ikke valid"));
      assertFalse("Kan ikke parse XML, det er ikke valid", true);
    }
  }

  @Test
  public void testPHMRXmlConvertermedcomKOLExample1() {
    cda = SetupMedcomKOLExample1.defineAsCDA();

    String expectedEncodedXml = null;
    PHMRDocument expectedDecoded = null;
    String expectedEncodedNew = null;

    try {
      expectedEncodedXml = encode(cda);
      byte ptext[] = expectedEncodedXml.getBytes();
      String value = new String(ptext, "UTF-8");
      expectedDecoded = decode(value);
      expectedEncodedNew = encode(expectedDecoded);
      compare(expectedEncodedXml, expectedEncodedNew);
    } catch (Exception e) {
      outXml(expectedEncodedXml, expectedEncodedNew, new StringBuffer("Kan ikke parse XML, det er ikke valid"));
      assertFalse("Kan ikke parse XML, det er ikke valid", true);
    }
  }

  @Test
  public void testPHMRXMLConverterOldNancy() {
    try {
      File file = new File(this.getClass().getClassLoader().getResource("phmr/PHMR_OLD_NANCY.xml").toURI());
      byte[] encoded = new byte[(int) file.length()];
      InputStream is = new FileInputStream(file);
      is.read(encoded, 0, (int) file.length());
      is.close();
      String phmrAsXML = new String(encoded, "UTF-8");
      decode(phmrAsXML);
    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  private PHMRDocument decode(String phmrAsXML) {
    long start = System.currentTimeMillis();
    PHMRDocument document = codec.decode(phmrAsXML);
    long end = System.currentTimeMillis();
    System.out.println("Duration: " + (end - start));
    return document;
  }

  private String encode(PHMRDocument pHMRDocument) {
    return codec.encode(pHMRDocument);
  }

  public void compare(String expectedXml, String computedXml) throws SAXException, IOException {
    // Use XMLDfiff for comparison, ignore differences in whitespace
    XMLUnit.setIgnoreWhitespace(true);
    XMLUnit.setIgnoreComments(true);
    XMLUnit.setIgnoreAttributeOrder(true);
    Diff xmlDiff = new Diff(expectedXml, computedXml);
    StringBuffer assertMessage = new StringBuffer(), detailedMessage = new StringBuffer();
    xmlDiff.overrideDifferenceListener(new DebugDifferenceListener(assertMessage, detailedMessage));
    boolean similar = xmlDiff.similar();

    // For debugging, you can output all the differences!
    if (!similar) {
      outXml(expectedXml, computedXml, detailedMessage);
    }

    assertTrue(assertMessage.toString(), similar);
  }

  private void outXml(String expectedXml, String computedXml, StringBuffer detailedMessage) {
    System.out.println("------------- Detailed Message --------------");
    System.out.print(HelperMethods.indentLines(detailedMessage.toString()));
    System.out.println("------------- Expected XML ------------------");
    System.out.print(HelperMethods.indentLines(expectedXml));
    System.out.println("------------- Computed XML ------------------");
    System.out.print(HelperMethods.indentLines(computedXml));
    System.out.println("------------- End of details ----------------");
  }

  private class DebugDifferenceListener implements DifferenceListener {

    private StringBuffer assertMessage;
    private StringBuffer detailedMessage;

    DebugDifferenceListener(StringBuffer assertMessage, StringBuffer detailedMessage) {
      this.assertMessage = assertMessage;
      this.detailedMessage = detailedMessage;
    }

    @Override
    public int differenceFound(Difference diff) {
      detailedMessage.append(diff.toString()).append("\n");
      assertMessage.append(diff.getDescription()).append("\n");
      return RETURN_ACCEPT_DIFFERENCE;
    }

    @Override
    public void skippedComparison(Node node1, Node node2) {

    }
  }
}
