package dk.s4.hl7.cda.convert;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.Difference;
import org.custommonkey.xmlunit.DifferenceListener;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import dk.s4.hl7.cda.convert.PHMRXmlCodec;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;
import dk.s4.hl7.cda.model.testutil.HelperMethods;

/**
 * Validate the output from our own PHMR builder with the example from MedCom.
 *
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */
public final class TestAgainstMedComExamples {

  @Test
  public void shouldMatchExpectedValueEx1() throws ParserConfigurationException, SAXException, IOException {
    // Create an PHMR document matching MedCom Example 1
    PHMRDocument cda = SetupMedcomExample1.defineAsCDA();

    // Load the MedCom example 1, MODIFIED as there are some defects in it
    shouldMatchExpectedValue(cda, "aa2386d0-79ea-11e3-981f-0800200c9a66", "Ex1-Weight_measurement-MODIFIED.xml");
  }

  @Test
  public void shouldMatchExpectedValueEx2() throws ParserConfigurationException, SAXException, IOException {
    // Create an PHMR document matching MedCom Example 2
    PHMRDocument cda = SetupMedcomExample2.defineAsCDA();

    // Load the MedCom example 2, MODIFIED as there are some defects in it
    shouldMatchExpectedValue(cda, "b6a079b0-89ab-11e3-baa8-0800200c9a66", "Ex2-Typing_error-MODIFIED.xml");
  }

  @Test
  public void shouldMatchExpectedValueEx4() throws ParserConfigurationException, SAXException, IOException {
    // Create an PHMR document matching MedCom Example 4
    PHMRDocument cda = SetupMedcomExample4.defineAsCDA();

    // Load the MedCom example 4, MODIFIED as there are some defects in it
    shouldMatchExpectedValue(cda, "437f8b60-9563-11e3-a5e2-0800200c9a66", "Ex4-COPD-MODIFIED.xml");
  }

  @Test
  public void shouldMatchExpectedValueExKOL1() throws ParserConfigurationException, SAXException, IOException {
    // Create an PHMR document matching MedCom KOL Example 1
    PHMRDocument cda = SetupMedcomKOLExample1.defineAsCDA();

    // Load the MedCom KOL example 1, MODIFIED
    shouldMatchExpectedValue(cda, "021ba3bd-6935-45ca-bdcd-ed77f8b2ee2a", "PHMR_KOL_Example_1_MaTIS-MODIFIED.xml");
  }

  private void shouldMatchExpectedValue(PHMRDocument cda, String uuid, String filename)
      throws ParserConfigurationException, SAXException, IOException {
    PHMRXmlCodec phmrCodec = new PHMRXmlCodec();

    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    // important! http://www.kdgregory.com/index.php?page=xml.parsing
    // factory.setNamespaceAware(true);
    DocumentBuilder xmlBuilder = factory.newDocumentBuilder();

    // Load the MedCom example
    // this requires the XML files to have an UTF-8 BOM
    Document expected = xmlBuilder.parse("src/test/resources/phmr/" + filename);

    // Generate the XML
    long start = System.currentTimeMillis();
    String xmlText = phmrCodec.encode(cda);
    long end = System.currentTimeMillis();
    System.out.println("Convert duration: " + (end - start));

    // Parse XML to dom and add stylesheet header in xml
    Document computed = HelperMethods.parseXMLStringToDOM(xmlText, true);
    HelperMethods.addStyleSheetHeader(computed);

    assertNotNull(computed);
    // Use XMLDfiff for comparison, ignore differences in whitespace
    XMLUnit.setIgnoreWhitespace(true);
    XMLUnit.setIgnoreComments(true);
    XMLUnit.setIgnoreAttributeOrder(true);
    Diff xmlDiff = new Diff(expected, computed);
    StringBuffer assertMessage = new StringBuffer(), detailedMessage = new StringBuffer();
    xmlDiff.overrideDifferenceListener(new DebugDifferenceListener(assertMessage, detailedMessage));
    boolean similar = xmlDiff.similar();

    // For debugging, you can output all the differences!
    if (!similar) {
      String computedXml = HelperMethods.convertXMLDocumentToString(computed);
      String expectedXml = HelperMethods.convertXMLDocumentToString(expected);

      System.out.println("------------- Detailed Message --------------");
      System.out.print(HelperMethods.indentLines(detailedMessage.toString()));
      System.out.println("------------- Expected XML ------------------");
      System.out.print(HelperMethods.indentLines(expectedXml));
      System.out.println("------------- Computed XML ------------------");
      System.out.print(HelperMethods.indentLines(computedXml));
      System.out.println("------------- End of details ----------------");
    }

    assertTrue(assertMessage.toString(), similar);
  }

  private class DebugDifferenceListener implements DifferenceListener {

    private StringBuffer assertMessage;
    private StringBuffer detailedMessage;

    DebugDifferenceListener(StringBuffer assertMessage, StringBuffer detailedMessage) {
      this.assertMessage = assertMessage;
      this.detailedMessage = detailedMessage;
    }

    @Override
    public int differenceFound(Difference diff) {
      detailedMessage.append(diff.toString()).append("\n");
      assertMessage.append(diff.getDescription()).append("\n");
      return RETURN_ACCEPT_DIFFERENCE;
    }

    @Override
    public void skippedComparison(Node node1, Node node2) {
      
    }
  }
}
