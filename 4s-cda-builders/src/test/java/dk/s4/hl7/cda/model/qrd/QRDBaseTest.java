package dk.s4.hl7.cda.model.qrd;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import dk.s4.hl7.cda.convert.QRDXmlConverter;
import dk.s4.hl7.cda.model.testutil.HelperMethods;

/**
 * Base class for testing QFDD
 *
 * @author Frank Jacobsen, Systematic
 */
public class QRDBaseTest {
  public Document qrdAsXML;
  public String asString;

  public String createQRDFromCDA(QRDDocument cda) throws ParserConfigurationException, SAXException, IOException {
    QRDXmlConverter converter = new QRDXmlConverter();
    long start = System.currentTimeMillis();
    String xmltext = converter.convert(cda);
    long end = System.currentTimeMillis();
    System.out.println("Duration: " + (end - start));
    qrdAsXML = HelperMethods.parseXMLStringToDOM(xmltext, false);
    return xmltext;
  }
}
