package dk.s4.hl7.cda.model.qfdd;

import java.util.Date;
import java.util.UUID;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.testutil.HelperMethods;
import dk.s4.hl7.cda.model.testutil.Setup;

/**
 * Helper methods to create a Text example.
 *
 * @author Frank Jacobsen, Systematic
 *
 */
public class SetupQFDDTextExample {

  public static QFDDDocument defineAsCDA() {
    QFDDDocument defineQFDDDocumentHeaderInformation = defineQFDDDocumentHeaderInformation();
    return defineQFDDDocumentHeaderInformation;
  }

  /** Define a CDA for the Medcom Text example */
  public static QFDDDocument defineQFDDDocumentHeaderInformation() {

    // Define the 'time'
    Date documentCreationTime = HelperMethods.makeDanishDateTime(2014, 0, 13, 10, 0, 0);

    // Setup Anders Andersen as authenticator
    PersonIdentity andersAndersen = new PersonIdentity.PersonBuilder("Andersen").addGivenName("Anders").build();

    // 1. Create a QFDD document as a "Green CDA", that is,
    // a data structure containing only the dynamic data
    // of a CDA.
    ID idHeader = new ID.IDBuilder()
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .setExtension(UUID.randomUUID().toString())
        .setRoot(MedCom.ROOT_OID)
        .build();
    QFDDDocument qfddDocument = new QFDDDocument(idHeader);
    qfddDocument.setTitle("SPØRGESKEMA 1 OM DIN EPILEPSI");
    qfddDocument.setLanguageCode("da-DK");

    // 1.1 Populate with time and version info
    qfddDocument.setDocumentVersion("2358344", 1);
    qfddDocument.setEffectiveTime(documentCreationTime);

    // 1.2 Populate the document with patient information
    Patient nancy = Setup.defineNancyAsFullPersonIdentity();
    qfddDocument.setPatient(nancy);

    // 1.3 Populate with Author, Custodian, and Authenticator
    // Setup Svendborg sygehus Hjertemedicinsk B as organization
    OrganizationIdentity custodianOrganization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("88878685")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .setAddress(Setup.defineHjerteMedicinskAfdAddress())
        .addTelecom(Use.WorkPlace, "tel", "65223344")
        .build();

    qfddDocument.setCustodian(custodianOrganization);

    OrganizationIdentity organization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("88878685")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .build();

    qfddDocument.setAuthor(new ParticipantBuilder()
        .setAddress(custodianOrganization.getAddress())
        .setSOR(custodianOrganization.getIdValue())
        .setTelecomList(custodianOrganization.getTelecomList())
        .setTime(documentCreationTime)
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(organization)
        .build());

    Date at1000onJan13 = HelperMethods.makeDanishDateTime(2014, 0, 13, 10, 0, 0);

    qfddDocument.setLegalAuthenticator(new ParticipantBuilder()
        .setAddress(custodianOrganization.getAddress())
        .setSOR(custodianOrganization.getIdValue())
        .setTelecomList(custodianOrganization.getTelecomList())
        .setTime(at1000onJan13)
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(organization)
        .build());

    // 1.4 Define the service period
    Date from = HelperMethods.makeDanishDateTime(2014, 0, 6, 8, 2, 0);
    Date to = HelperMethods.makeDanishDateTime(2014, 0, 10, 8, 15, 0);
    qfddDocument.setDocumentationTimeInterval(from, to);

    String text = "OM DETTE SKEMA: "
        + "Vi bruger blandt andet dine svar til at vurdere, om du har brug for en konsultation. <br/>"
        + "Hvornår havde du dit seneste anfald?";

    Section<QFDDQuestion> section = new Section<QFDDQuestion>("Indledning", text);

    CodedValue codeValue = new CodedValue("value1", "value2", "value3", "value4");
    ID id = new ID.IDBuilder().setRoot("IDroot").setExtension("idExtension").setAuthorityName("AuthorityName").build();

    QFDDQuestion qfddQuestion = new QFDDTextQuestion.QFDDTextQuestionBuilder()
        .setCodeValue(codeValue)
        .setId(id)
        .setQuestion("Question")
        .build();

    section.addQuestionnaireEntity(qfddQuestion);

    qfddDocument.addSection(section);

    return qfddDocument;
  }
}
