package dk.s4.hl7.cda.convert;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.Difference;
import org.custommonkey.xmlunit.DifferenceListener;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Test;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID.IDBuilder;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.Participant.ROLE_TYPE;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.SectionInformation;
import dk.s4.hl7.cda.model.qrd.QRDAnalogSliderResponse.QRDAnalogSliderResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDDiscreteSliderResponse.QRDDiscreteSliderResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDDocument;
import dk.s4.hl7.cda.model.qrd.QRDMultipleChoiceResponse.QRDMultipleChoiceResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDResponse;
import dk.s4.hl7.cda.model.qrd.QRDTextResponse.QRDTextResponseBuilder;
import dk.s4.hl7.cda.model.qrd.SetupQRDNumericExample;
import dk.s4.hl7.cda.model.testutil.HelperMethods;
import dk.s4.hl7.cda.model.testutil.Setup;

public final class TestXmlToQrd {
  private QRDXmlCodec codec = new QRDXmlCodec();
  private QRDDocument cda;

  @Test
  public void testQRDXmlConverterNumericExample() throws Exception {
    cda = SetupQRDNumericExample.defineAsCDA();
    addAdditionalRespones(cda);
    addExtraParticipants(cda);
    cda.getSections().add(0, new Section<QRDResponse>(new SectionInformation("vejledning", "vejledning")));

    String expectedEncodedXml = null;
    QRDDocument expectedDecoded = null;
    String expectedEncodedNew = null;

    try {
      expectedEncodedXml = encode(cda);
      byte ptext[] = expectedEncodedXml.getBytes();
      String value = new String(ptext);
      expectedDecoded = decode(value);
      expectedEncodedNew = encode(expectedDecoded);
      compare(expectedEncodedXml, expectedEncodedNew);
    } catch (Exception e) {
      outXml(expectedEncodedXml, expectedEncodedNew, new StringBuffer("Kan ikke parse XML, det er ikke valid"));
      assertFalse("Kan ikke parse XML, det er ikke valid", true);
    }
  }

  private void addExtraParticipants(QRDDocument cdaInner) {
    PersonIdentity andersAndersen = new PersonIdentity.PersonBuilder("Andersen").addGivenName("Anders").build();
    OrganizationIdentity custodianOrganization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("310541000016007")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .setAddress(Setup.defineHjerteMedicinskAfdAddress())
        .addTelecom(Use.WorkPlace, "tel", "65223344")
        .build();

    for (int i = 0; i < 2; i++) {
      cdaInner.addInformationRecipients(new ParticipantBuilder()
          .setTelecomList(custodianOrganization.getTelecomList())
          .setSOR("31054100001600" + i)
          .setPersonIdentity(andersAndersen)
          .setOrganizationIdentity(custodianOrganization)
          .build());
    }

    cdaInner.setDataEnterer(new ParticipantBuilder()
        .setAddress(custodianOrganization.getAddress())
        .setSOR(custodianOrganization.getIdValue())
        .setTelecomList(custodianOrganization.getTelecomList())
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(custodianOrganization)
        .build());

    for (int i = 0; i < 2; i++) {
      cdaInner.addParticipant(new ParticipantBuilder()
          .setRoleType(ROLE_TYPE.CAREGIVER)
          .setTelecomList(custodianOrganization.getTelecomList())
          .setSOR("31054100001600" + i)
          .setPersonIdentity(andersAndersen)
          .setOrganizationIdentity(custodianOrganization)
          .build());
    }
  }

  private void addAdditionalRespones(QRDDocument qrdDocument) {
    Section<QRDResponse> section = qrdDocument.getSections().get(0);

    section.addQuestionnaireEntity(new QRDAnalogSliderResponseBuilder()
        .setCodeValue(new CodedValue("code1", "codeSystem1", "displayName1", "codeSystemName1"))
        .setId(new IDBuilder().setRoot("root1").setExtension("extension1").setAuthorityName("authorityName1").build())
        .setInterval("0", "120", "2")
        .setQuestion("question1")
        .setValue("10", "INT")
        .build());

    section.addQuestionnaireEntity(new QRDAnalogSliderResponseBuilder()
        .setCodeValue(new CodedValue("code2", "codeSystem2", "displayName2", "codeSystemName2"))
        .setId(new IDBuilder().setRoot("root2").setExtension("extension2").setAuthorityName("authorityName2").build())
        .setInterval("100", "200", "4")
        .setQuestion("qeustion2")
        .setValue("20", "INT")
        .build());

    section.addQuestionnaireEntity(new QRDDiscreteSliderResponseBuilder()
        .setCodeValue(new CodedValue("code1", "codeSystem1", "displayName1", "codeSystemName1"))
        .setId(new IDBuilder().setRoot("root1").setExtension("extension1").setAuthorityName("authorityName1").build())
        .setMinimum(1)
        .setQuestion("question1")
        .setAnswer("answerCode1", "answerCodeSystem1", "answerDisplayName1", "answerCodeSystemName1")
        .build());

    section.addQuestionnaireEntity(new QRDDiscreteSliderResponseBuilder()
        .setCodeValue(new CodedValue("code2", "codeSystem2", "displayName2", "codeSystemName2"))
        .setId(new IDBuilder().setRoot("root2").setExtension("extension2").setAuthorityName("authorityName2").build())
        .setMinimum(1)
        .setQuestion("question2")
        .setAnswer("answerCode2", "answerCodeSystem2", "answerDisplayName2", "answerCodeSystemName2")
        .build());

    section = new Section<QRDResponse>("testingTitle2", "testingText2");

    section.addQuestionnaireEntity(new QRDMultipleChoiceResponseBuilder()
        .setCodeValue(new CodedValue("code1", "codeSystem1", "displayName1", "codeSystemName1"))
        .setId(new IDBuilder().setRoot("root1").setExtension("extension1").setAuthorityName("authorityName1").build())
        .setMinimum("1")
        .setMaximum("2")
        .setQuestion("question1")
        .addAnswer("answerCode1", "answerCodeSystem1", "answerDisplayName1", "answerCodeSystemName1")
        .addAnswer("answerCode2", "answerCodeSystem2", "answerDisplayName2", "answerCodeSystemName2")
        .build());

    section.addQuestionnaireEntity(new QRDMultipleChoiceResponseBuilder()
        .setCodeValue(new CodedValue("code2", "codeSystem2", "displayName2", "codeSystemName2"))
        .setId(new IDBuilder().setRoot("root2").setExtension("extension2").setAuthorityName("authorityName2").build())
        .setMinimum("3")
        .setMaximum("5")
        .setQuestion("question2")
        .addAnswer("answerCode3", "answerCodeSystem3", "answerDisplayName3", "answerCodeSystemName3")
        .addAnswer("answerCode4", "answerCodeSystem4", "answerDisplayName4", "answerCodeSystemName4")
        .build());

    section.addQuestionnaireEntity(new QRDTextResponseBuilder()
        .setCodeValue(new CodedValue("code1", "codeSystem1", "displayName1", "codeSystemName1"))
        .setId(new IDBuilder().setRoot("root1").setExtension("extension1").setAuthorityName("authorityName1").build())
        .setQuestion("question1")
        .setText("answer1")
        .build());

    section.addQuestionnaireEntity(new QRDTextResponseBuilder()
        .setCodeValue(new CodedValue("code2", "codeSystem2", "displayName2", "codeSystemName2"))
        .setId(new IDBuilder().setRoot("root2").setExtension("extension2").setAuthorityName("authorityName2").build())
        .setQuestion("question2")
        .setText("answer2")
        .build());

    qrdDocument.addSection(section);
  }

  private QRDDocument decode(String qrdAsXML) {
    long start = System.currentTimeMillis();
    QRDDocument document = codec.decode(qrdAsXML);
    long end = System.currentTimeMillis();
    System.out.println("Duration: " + (end - start));
    return document;
  }

  private String encode(QRDDocument qrdDocument) {
    return codec.encode(qrdDocument);
  }

  public void compare(String expectedXml, String computedXml) throws SAXException, IOException {
    // Use XMLDfiff for comparison, ignore differences in whitespace
    XMLUnit.setIgnoreWhitespace(true);
    XMLUnit.setIgnoreComments(true);
    XMLUnit.setIgnoreAttributeOrder(true);
    Diff xmlDiff = new Diff(expectedXml, computedXml);
    StringBuffer assertMessage = new StringBuffer(), detailedMessage = new StringBuffer();
    xmlDiff.overrideDifferenceListener(new DebugDifferenceListener(assertMessage, detailedMessage));
    boolean similar = xmlDiff.similar();

    // For debugging, you can output all the differences!
    if (!similar) {
      outXml(expectedXml, computedXml, detailedMessage);
    }

    assertTrue(assertMessage.toString(), similar);
  }

  private void outXml(String expectedXml, String computedXml, StringBuffer detailedMessage) {
    System.out.println("------------- Detailed Message --------------");
    System.out.print(HelperMethods.indentLines(detailedMessage.toString()));
    System.out.println("------------- Expected XML ------------------");
    System.out.print(HelperMethods.indentLines(expectedXml));
    System.out.println("------------- Computed XML ------------------");
    System.out.print(HelperMethods.indentLines(computedXml));
    System.out.println("------------- End of details ----------------");
  }

  private class DebugDifferenceListener implements DifferenceListener {

    private StringBuffer assertMessage;
    private StringBuffer detailedMessage;

    DebugDifferenceListener(StringBuffer assertMessage, StringBuffer detailedMessage) {
      this.assertMessage = assertMessage;
      this.detailedMessage = detailedMessage;
    }

    @Override
    public int differenceFound(Difference diff) {
      detailedMessage.append(diff.toString()).append("\n");
      assertMessage.append(diff.getDescription()).append("\n");
      return RETURN_ACCEPT_DIFFERENCE;
    }

    @Override
    public void skippedComparison(Node node1, Node node2) {

    }
  }
}
