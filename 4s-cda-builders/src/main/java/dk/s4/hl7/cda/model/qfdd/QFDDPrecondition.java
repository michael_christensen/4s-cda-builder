package dk.s4.hl7.cda.model.qfdd;

import dk.s4.hl7.cda.convert.decode.CdaBuilderException;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.util.ModelUtil;

public class QFDDPrecondition {
  private final CodedValue codeValue;
  private String valueType;

  // min and max only set for numeric range
  private final String minimum;
  private final String maximum;

  // CodedValue is set for non numeric range
  private final CodedValue answer;

  private QFDDPrecondition(QFDDPreconditionBuilder builder) {
    this.codeValue = builder.codeValue;
    this.minimum = builder.minimum;
    this.maximum = builder.maximum;
    this.answer = builder.answer;
    this.valueType = builder.valueType;
  }

  public CodedValue getCodeValue() {
    return codeValue;
  }

  public String getMinimum() {
    return minimum;
  }

  public String getMaximum() {
    return maximum;
  }

  public String getValueType() {
    return valueType;
  }

  public CodedValue getAnswer() {
    return answer;
  }

  public static class QFDDPreconditionBuilder {
    private CodedValue codeValue;
    private String minimum;
    private String maximum;
    private CodedValue answer;
    private String valueType;

    public QFDDPreconditionBuilder(CodedValue codeValue) {
      this.codeValue = codeValue;
    }

    public QFDDPreconditionBuilder setMinimum(String minimum) {
      ModelUtil.checkNull(minimum, "Minimum must have a value");
      this.minimum = minimum;
      return this;
    }

    public QFDDPreconditionBuilder setMaximum(String maximum) {
      ModelUtil.checkNull(maximum, "Maximum must have a value");
      this.maximum = maximum;
      return this;
    }

    public QFDDPreconditionBuilder setAnswer(CodedValue answer) {
      ModelUtil.checkNull(answer, "Answer code must have a value");
      this.valueType = "CE";
      this.answer = answer;
      return this;
    }

    public QFDDPrecondition build() {
      if ((minimum == null || maximum == null) && answer == null) {
        throw new CdaBuilderException("Precondition must have an interval or an answer");
      }
      return new QFDDPrecondition(this);
    }

    public QFDDPreconditionBuilder setValueType(String valueType) {
      this.valueType = valueType;
      return this;
    }
  }
}
