package dk.s4.hl7.cda.model.qfdd;

import dk.s4.hl7.cda.model.util.ModelUtil;

public class QFDDHelpText {
  private String language;
  private final String helpText;

  private QFDDHelpText(QFDDHelpTextBuilder builder) {
    this.language = builder.language;
    this.helpText = builder.helpText;
  }

  public static class QFDDHelpTextBuilder {
    private String language;
    private String helpText;

    public QFDDHelpTextBuilder language(String language) {
      this.language = language;
      return this;
    }

    public QFDDHelpTextBuilder helpText(String helpText) {
      this.helpText = helpText;
      return this;
    }

    public QFDDHelpText build() {
      ModelUtil.checkNull(helpText, "Help text is mandatory");
      return new QFDDHelpText(this);
    }
  }

  public String getLanguage() {
    return language;
  }

  public String getHelpText() {
    return helpText;
  }
}
