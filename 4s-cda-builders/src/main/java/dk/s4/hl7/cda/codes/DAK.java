package dk.s4.hl7.cda.codes;

import java.util.Date;

import dk.s4.hl7.cda.model.*;
import dk.s4.hl7.cda.model.phmr.Measurement;

//CHECKSTYLE:OFF
public class DAK {

  public static final String DISPLAYNAME = "DAK-e datafangst";
  public static final String CODESYSTEM_OID = "1.2.208.184.100.8";

  public static final String FVC_CODE = "MCS88016";
  public static final String DAK_FVC_DISPLAYNAME = "FVC";

  public static Measurement createFVC(String fvc, Date atTime, DataInputContext context, ID id) {
    return new Measurement.MeasurementBuilder(atTime, Measurement.Status.COMPLETED)
        .useAlternativeCodingSystem(DAK.CODESYSTEM_OID, DAK.DISPLAYNAME)
        .setPhysicalQuantity(fvc, UCUM.L, DAK.FVC_CODE, DAK_FVC_DISPLAYNAME)
        .setContext(context)
        .setId(id)
        .build();
  }
}
