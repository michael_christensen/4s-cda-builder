package dk.s4.hl7.cda.convert.encode.pattern;

import java.io.IOException;
import java.util.List;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.convert.encode.BuildUtil;
import dk.s4.hl7.cda.model.Reference;
import dk.s4.hl7.util.xml.XmlStreamBuilder;

public class ExternalReferencePattern {
  public void build(List<Reference> referenceList, XmlStreamBuilder xmlBuilder) throws IOException {
    if (referenceList == null || referenceList.isEmpty()) {
      return;
    }
    for (Reference reference : referenceList) {
      xmlBuilder.element("reference").attribute("typeCode", "REFR");
      BuildUtil.buildTemplateIds(xmlBuilder, MedCom.DK_REFERENCE_ROOT_ID);
      if (reference.getExternalObservation() == null) {
        xmlBuilder.element("externalDocument").attribute("classCode", "DOC");
      } else {
        xmlBuilder.element("externalObservation").attribute("classCode", "OBS");
      }

      BuildUtil.buildId(reference.getExternalDocument().getRoot(), reference.getExternalDocument().getExtension(),
          xmlBuilder);
      BuildUtil.buildId(MedCom.DK_REFERENCE_EXTERNAL_DOCUMENT, reference.getReferencesUse(), xmlBuilder);
      if (reference.getExternalObservation() != null) {
        BuildUtil.buildId(reference.getExternalObservation().getRoot(), reference
            .getExternalObservation()
            .getExtension(), xmlBuilder);
      }
      BuildUtil.buildCode(reference.getExternalDocumentType().getCode(), reference
          .getExternalDocumentType()
          .getCodeSystem(), reference.getExternalDocumentType().getDisplayName(), xmlBuilder);

      xmlBuilder.elementEnd(); // end externalDocument or externalObservation
      xmlBuilder.elementEnd(); // end reference
    }
  }
}
