package dk.s4.hl7.cda.model.qfdd;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.SectionInformation;
import dk.s4.hl7.cda.model.core.BaseClinicalDocument;

public class QFDDDocument extends BaseClinicalDocument {
  private List<Section<QFDDQuestion>> sections;
  private SectionInformation copyRight;

  public QFDDDocument(ID id) {
    super(HL7.CDA_TYPEID_ROOT, HL7.CDA_TYPEID_EXTENSION, id, Loinc.QFD_CODE, Loinc.QFD_DISPLAYNAME,
        new String[] { MedCom.DK_QFD_ROOT_OID, MedCom.DK_QFD_ROOT_OID_LEVEL }, HL7.REALM_CODE_DK);
    this.sections = new ArrayList<Section<QFDDQuestion>>();
  }

  public List<Section<QFDDQuestion>> getSections() {
    return sections;
  }

  public void addSection(Section<QFDDQuestion> section) {
    sections.add(section);
  }

  public SectionInformation getCopyRight() {
    return copyRight;
  }

  public void setCopyRight(SectionInformation copyRight) {
    this.copyRight = copyRight;
  }
}