package dk.s4.hl7.cda.convert.decode.general;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import dk.s4.hl7.cda.codes.BasicType;
import dk.s4.hl7.util.xml.XMLElement;

public class ValueCollection {
  private List<List<String>> values;

  public enum CE_INDEX {
    TYPE, CODE, CODE_SYSTEM, DISPLAY_NAME, CODE_SYSTEM_NAME
  }

  public enum PQ_INDEX {
    TYPE, VALUE
  }

  public enum ST_INDEX {
    TYPE, TEXT
  }

  public ValueCollection() {
    values = new ArrayList<List<String>>();
  }

  public void addValue(XMLElement xmlElement) {
    String type = xmlElement.getAttributeValue("type");
    if (type.equalsIgnoreCase("CE")) {
      values.add(Arrays.asList(xmlElement.getAttributeValue("type"), xmlElement.getAttributeValue("code"),
          xmlElement.getAttributeValue("codeSystem"), xmlElement.getAttributeValue("displayName"),
          xmlElement.getAttributeValue("codeSystemName")));
    } else if (type.equalsIgnoreCase("ST")) {
      values.add(Arrays.asList(xmlElement.getAttributeValue("type"), xmlElement.getElementValue()));
    } else if (type.equalsIgnoreCase("PQ")) {
      addPhysicalQuantity(xmlElement);
    } else if (type.equalsIgnoreCase(BasicType.INT.name())) {
      addPhysicalQuantity(xmlElement);
    } else if (type.equalsIgnoreCase(BasicType.REAL.name())) {
      addPhysicalQuantity(xmlElement);
    } else if (type.equalsIgnoreCase(BasicType.TS.name())) {
      addPhysicalQuantity(xmlElement);
    }
  }

  private void addPhysicalQuantity(XMLElement xmlElement) {
    values.add(Arrays.asList(xmlElement.getAttributeValue("type"), xmlElement.getAttributeValue("value")));
  }

  public List<String> getSingleValue() {
    if (values.isEmpty()) {
      return new ArrayList<String>();
    }
    return values.get(0);
  }

  public List<List<String>> getValues() {
    return values;
  }

  public void clear() {
    values.clear();
  }
}
