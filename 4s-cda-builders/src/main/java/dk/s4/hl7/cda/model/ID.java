package dk.s4.hl7.cda.model;

/**
 * 
 * General ID class for holding the ID. The actual value of the ID is the
 * extension attribute
 *
 */
public class ID {
  private final String extension;
  private final String root;
  private final String authorityName;

  private ID(IDBuilder builder) {
    this.extension = builder.extension;
    this.root = builder.root;
    this.authorityName = builder.authorityName;
  }

  public String getExtension() {
    return extension;
  }

  public String getRoot() {
    return root;
  }

  public String getAuthorityName() {
    return authorityName;
  }

  public static class IDBuilder {
    private String extension;
    private String root;
    private String authorityName;

    public IDBuilder setExtension(String extension) {
      this.extension = extension;
      return this;
    }

    public IDBuilder setRoot(String root) {
      this.root = root;
      return this;
    }

    public IDBuilder setAuthorityName(String authorityName) {
      this.authorityName = authorityName;
      return this;
    }

    public ID build() {
      return new ID(this);
    }
  }
}
