package dk.s4.hl7.cda.convert.decode.qfdd;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.convert.decode.general.BaseXmlHandler;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.CodedValue.CodedValueBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDPrecondition;
import dk.s4.hl7.cda.model.qfdd.QFDDPrecondition.QFDDPreconditionBuilder;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class PreconditionHandler extends BaseXmlHandler {
  private CodedValue code;
  private String minimum;
  private String maximum;
  private String valueType;
  private CodedValue answer;
  private List<QFDDPrecondition> preconditions;

  public PreconditionHandler(String preconditionBase) {
    this.preconditions = new ArrayList<QFDDPrecondition>();
    addPath(preconditionBase);
    addPath(preconditionBase + "/templateId");
    addPath(preconditionBase + "/criterion/code");
    addPath(preconditionBase + "/criterion/value");
    addPath(preconditionBase + "/criterion/value/low");
    addPath(preconditionBase + "/criterion/value/high");
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "code", "code", "codeSystem")) {
      code = new CodedValueBuilder()
          .setCode(xmlElement.getAttributeValue("code"))
          .setCodeSystem(xmlElement.getAttributeValue("codeSystem"))
          .setDisplayName(xmlElement.getAttributeValue("displayName"))
          .setCodeSystemName(xmlElement.getAttributeValue("codeSystemName"))
          .build();
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "value", "type")) {
      valueType = xmlElement.getAttributeValue("type");
      if ("CE".equalsIgnoreCase(valueType)) {
        answer = new CodedValueBuilder()
            .setCode(xmlElement.getAttributeValue("code"))
            .setCodeSystem(xmlElement.getAttributeValue("codeSystem"))
            .setDisplayName(xmlElement.getAttributeValue("displayName"))
            .setCodeSystemName(xmlElement.getAttributeValue("codeSystemName"))
            .build();
      }
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "low", "value")) {
      minimum = xmlElement.getAttributeValue("value");
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "high", "value")) {
      maximum = xmlElement.getAttributeValue("value");
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "precondition")) {
      if (answer == null) {
        preconditions.add(new QFDDPreconditionBuilder(code)
            .setMinimum(minimum)
            .setMaximum(maximum)
            .setValueType(valueType)
            .build());
      } else {
        preconditions.add(new QFDDPreconditionBuilder(code).setAnswer(answer).setValueType(valueType).build());
      }
      localClear();
    }
  }

  private void localClear() {
    code = null;
    minimum = null;
    maximum = null;
    valueType = null;
    answer = null;
  }

  public void clear() {
    localClear();
    preconditions.clear();
  }

  public List<QFDDPrecondition> getPreconditions() {
    return preconditions;
  }

  public boolean hasValues() {
    return !preconditions.isEmpty();
  }
}
