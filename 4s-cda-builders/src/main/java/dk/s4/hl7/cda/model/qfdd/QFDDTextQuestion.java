package dk.s4.hl7.cda.model.qfdd;

public class QFDDTextQuestion extends QFDDQuestion {
  public static class QFDDTextQuestionBuilder extends
      QFDDQuestion.BaseQFDDQuestionBuilder<QFDDTextQuestion, QFDDTextQuestionBuilder> {
    public QFDDTextQuestionBuilder() {
    }

    @Override
    public QFDDTextQuestionBuilder getThis() {
      return this;
    }

    @Override
    public QFDDTextQuestion build() {
      return new QFDDTextQuestion(this);
    }
  }

  private QFDDTextQuestion(QFDDTextQuestionBuilder builder) {
    super(builder);
  }
}
