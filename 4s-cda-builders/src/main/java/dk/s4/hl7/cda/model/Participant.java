package dk.s4.hl7.cda.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import dk.s4.hl7.cda.codes.NSI;
import dk.s4.hl7.cda.model.AddressData.Use;

public class Participant {
  private ID id;
  private ROLE_TYPE roleType;
  private AddressData address;
  private Telecom[] telecom;
  private Date time;
  private PersonIdentity personIdentity;
  private OrganizationIdentity organizationIdentity;

  public enum ROLE_TYPE {
    PRS, // Personal relationship
    NOK, // Next of kin
    CAREGIVER, // Caregiver
    AGNT, // Agent
    GUAR, // Guardian
    ECON // Emergency contact
  }

  public Participant(ParticipantBuilder participantBuilder) {
    this.id = participantBuilder.id;
    this.roleType = participantBuilder.roleType;
    this.address = participantBuilder.address;
    this.time = participantBuilder.time;
    this.personIdentity = participantBuilder.personIdentity;
    this.organizationIdentity = participantBuilder.organizationIdentity;

    if (participantBuilder.telecomList != null && !participantBuilder.telecomList.isEmpty()) {
      this.telecom = participantBuilder.telecomList.toArray(new Telecom[participantBuilder.telecomList.size()]);
    } else {
      this.telecom = new Telecom[0];
    }
  }
  
  public ROLE_TYPE getRoleType() {
    return roleType;
  }

  public PersonIdentity getPersonIdentity() {
    return personIdentity;
  }

  public OrganizationIdentity getOrganizationIdentity() {
    return organizationIdentity;
  }

  public Date getTime() {
    return time;
  }

  public ID getId() {
    return id;
  }

  public AddressData getAddress() {
    return address;
  }

  public Telecom[] getTelecomList() {
    return telecom;
  }

  public static class ParticipantBuilder {
    private ROLE_TYPE roleType;
    private ID id;
    private AddressData address;
    private List<Telecom> telecomList;
    private Date time;
    private PersonIdentity personIdentity;
    private OrganizationIdentity organizationIdentity;

    public ParticipantBuilder() {
      this.telecomList = new ArrayList<Telecom>();
    }

    public ParticipantBuilder setPersonIdentity(PersonIdentity personIdentity) {
      this.personIdentity = personIdentity;
      return this;
    }

    public ParticipantBuilder setOrganizationIdentity(OrganizationIdentity organizationIdentity) {
      this.organizationIdentity = organizationIdentity;
      return this;
    }

    public ParticipantBuilder setTime(Date time) {
      this.time = time;
      return this;
    }

    public ParticipantBuilder setId(ID id) {
      this.id = id;
      return this;
    }

    public ParticipantBuilder setSOR(String sor) {
      this.id = NSI.createSOR(sor);
      return this;
    }

    public ParticipantBuilder setYDER(String yder) {
      this.id = NSI.createYDER(yder);
      return this;
    }

    public ParticipantBuilder setCPR(String cpr) {
      this.id = NSI.createCPR(cpr);
      return this;
    }
    
    public ParticipantBuilder setRoleType(ROLE_TYPE roleType) {
      this.roleType = roleType;
      return this;
    }

    public ParticipantBuilder setAddress(AddressData address) {
      this.address = address;
      return this;
    }

    public ParticipantBuilder addTelecom(Use use, String protocol, String telecom) {
      telecomList.add(new Telecom(use, protocol, telecom));
      return this;
    }

    public ParticipantBuilder setTelecomList(Telecom[] telecoms) {
      List<Telecom> telecomTemp = new ArrayList<Telecom>();
      if (telecoms != null) {
        for (Telecom telecom : telecoms) {
          telecomTemp.add(telecom);
        }
      }
      this.telecomList = telecomTemp;
      return this;
    }

    public ParticipantBuilder setTelecomList(List<Telecom> telecom) {
      this.telecomList = telecom;
      return this;
    }

    public Participant build() {
      return new Participant(this);
    }
  }
}
