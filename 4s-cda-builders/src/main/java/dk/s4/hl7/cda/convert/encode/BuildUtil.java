package dk.s4.hl7.cda.convert.encode;

import java.io.IOException;
import java.util.List;

import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.QuestionnaireEntity;
import dk.s4.hl7.util.xml.XmlStreamBuilder;

public class BuildUtil {

  public static void buildTemplateIds(XmlStreamBuilder xmlBuilder, String... templateIds) throws IOException {
    for (String templateId : templateIds) {
      xmlBuilder.element("templateId").attribute("root", templateId).elementShortEnd();
    }
  }

  public static void buildTemplateIds(XmlStreamBuilder xmlBuilder, List<String> templateIds) throws IOException {
    for (String templateId : templateIds) {
      xmlBuilder.element("templateId").attribute("root", templateId).elementShortEnd();
    }
  }

  public static void buildId(String root, String authorityName, String extension, XmlStreamBuilder xmlBuilder)
      throws IOException {
    if (extension != null && root != null) {
      xmlBuilder.element("id").attribute("root", root).attribute("extension", extension);
      if (authorityName != null) {
        xmlBuilder.attribute("assigningAuthorityName", authorityName);
      }
      xmlBuilder.elementShortEnd();
    } else {
      buildNullFlavor("id", xmlBuilder);
    }
  }

  public static void buildId(ID id, XmlStreamBuilder xmlBuilder) throws IOException {
    if (id != null && id.getExtension() != null && id.getRoot() != null) {
      xmlBuilder.element("id").attribute("root", id.getRoot()).attribute("extension", id.getExtension());
      if (id.getAuthorityName() != null) {
        xmlBuilder.attribute("assigningAuthorityName", id.getAuthorityName());
      }
      xmlBuilder.elementShortEnd();
    } else {
      buildNullFlavor("id", xmlBuilder);
    }
  }

  public static void buildId(String root, String extension, XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.element("id").attribute("root", root).attribute("extension", extension).elementShortEnd();
  }

  public static void buildNullFlavor(String elementName, XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.element(elementName).attribute("nullFlavor", "NI").elementShortEnd();
  }

  public static void buildQuestionnaireEntityCode(QuestionnaireEntity questionnaireEntity, XmlStreamBuilder xmlBuilder)
      throws IOException {
    CodedValue code = questionnaireEntity.getCode();
    if (code != null) {
      xmlBuilder
          .element("code")
          .attribute("code", code.getCode())
          .attribute("displayName", code.getDisplayName())
          .attribute("codeSystem", code.getCodeSystem())
          .attribute("codeSystemName", code.getCodeSystemName());
      xmlBuilder.element("originalText").value(questionnaireEntity.getQuestion()).elementEnd();
      xmlBuilder.elementEnd();
    }
  }

  public static void buildCode(String code, String codeSystem, String displayName, String codeSystemName,
      XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.element("code").attribute("code", code).attribute("codeSystem", codeSystem);
    if (displayName != null) {
      xmlBuilder.attribute("displayName", displayName);
    }
    if (codeSystemName != null) {
      xmlBuilder.attribute("codeSystemName", codeSystemName);
    }
    xmlBuilder.elementShortEnd();
  }

  public static void buildCode(String code, String codeSystem, XmlStreamBuilder xmlBuilder) throws IOException {
    buildCode(code, codeSystem, null, null, xmlBuilder);
  }

  public static void buildCode(String code, String codeSystem, String displayName, XmlStreamBuilder xmlBuilder)
      throws IOException {
    buildCode(code, codeSystem, displayName, null, xmlBuilder);
  }

  public static void buildCode(CodedValue codedValue, XmlStreamBuilder xmlBuilder) throws IOException {
    buildCode(codedValue.getCode(), codedValue.getCodeSystem(), codedValue.getDisplayName(),
        codedValue.getCodeSystemName(), xmlBuilder);
  }

}
