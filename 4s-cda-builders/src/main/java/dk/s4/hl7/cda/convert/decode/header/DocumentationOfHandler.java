package dk.s4.hl7.cda.convert.decode.header;

import java.util.Date;

import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.model.core.ClinicalDocument;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class DocumentationOfHandler implements CDAXmlHandler<ClinicalDocument> {
  private Date effectiveTimeLow;
  private Date effectiveTimeHigh;

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "low", "value")) {
      effectiveTimeLow = ConvertXmlUtil.getDateFromyyyyMMddhhmmss(xmlElement.getAttributeValue("value"));

    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "high", "value")) {
      effectiveTimeHigh = ConvertXmlUtil.getDateFromyyyyMMddhhmmss(xmlElement.getAttributeValue("value"));
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
  }
  
  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    xmlMapping.add("/ClinicalDocument/documentationOf/serviceEvent/effectiveTime/low", this);
    xmlMapping.add("/ClinicalDocument/documentationOf/serviceEvent/effectiveTime/high", this);
  }

  @Override
  public void addDataToDocument(ClinicalDocument clinicalDocument) {
    clinicalDocument.setDocumentationTimeInterval(effectiveTimeLow, effectiveTimeHigh);
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    xmlMapping.remove("/ClinicalDocument/documentationOf/serviceEvent/effectiveTime/low");
    xmlMapping.remove("/ClinicalDocument/documentationOf/serviceEvent/effectiveTime/high");
  }
}