package dk.s4.hl7.cda.convert.decode.qfdd;

import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.convert.decode.general.BaseXmlHandler;
import dk.s4.hl7.cda.model.qfdd.QFDDFeedback;
import dk.s4.hl7.cda.model.qfdd.QFDDFeedback.QFDDFeedbackBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDPrecondition;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class FeedbackHandler extends BaseXmlHandler {
  public static final String FEEDBACK_BASE = "/ClinicalDocument/component/structuredBody/component/section/entry/organizer/component/observation/entryRelationship/observation";

  private String feedbackText;
  private String language;
  private PreconditionHandler preconditionHandler;

  public FeedbackHandler() {
    this.preconditionHandler = new PreconditionHandler(FEEDBACK_BASE + "/precondition");
    addPath(FEEDBACK_BASE + "/value");
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "value", "type")) {
      if ("ST".equalsIgnoreCase(xmlElement.getAttributeValue("type"))) {
        feedbackText = xmlElement.getElementValue();
      }
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "value", "language")) {
      language = xmlElement.getAttributeValue("language");
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    // Ignored
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    super.addHandlerToMap(xmlMapping);
    preconditionHandler.addHandlerToMap(xmlMapping);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    super.removeHandlerFromMap(xmlMapping);
    preconditionHandler.removeHandlerFromMap(xmlMapping);
  }

  public QFDDFeedback getFeedback() {
    if (hasValues()) {
      QFDDFeedbackBuilder builder = new QFDDFeedbackBuilder().feedBackText(feedbackText).language(language);
      if (preconditionHandler.hasValues()) {
        for (QFDDPrecondition precondition : preconditionHandler.getPreconditions()) {
          builder.addPrecondition(precondition);
        }
      }
      return builder.build();
    }
    return null;
  }

  private boolean hasValues() {
    // Language is optional
    return feedbackText != null;
  }

  public void clear() {
    feedbackText = null;
    language = null;
    preconditionHandler.clear();
  }
}
