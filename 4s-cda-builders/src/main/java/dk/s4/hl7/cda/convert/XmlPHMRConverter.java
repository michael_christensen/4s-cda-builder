package dk.s4.hl7.cda.convert;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.ClinicalDocumentXmlConverter;
import dk.s4.hl7.cda.convert.decode.header.CDAHeaderHandler;
import dk.s4.hl7.cda.convert.decode.header.CustodianHandler;
import dk.s4.hl7.cda.convert.decode.header.DocumentationOfHandler;
import dk.s4.hl7.cda.convert.decode.header.PatientHandler;
import dk.s4.hl7.cda.convert.decode.phmr.SectionHandler;
import dk.s4.hl7.cda.model.core.ClinicalDocument;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;

public class XmlPHMRConverter extends ClinicalDocumentXmlConverter<PHMRDocument> {
  public PHMRDocument convert(String source) {
    return deserialize(new StringReader(source));
  }

  @Override
  protected List<CDAXmlHandler<ClinicalDocument>> createParticipantHandlers() {
    List<CDAXmlHandler<ClinicalDocument>> list = new ArrayList<CDAXmlHandler<ClinicalDocument>>();
    list.add(new PatientHandler());
    list.add(createAuthorHandler());
    list.add(createLegalAuthenticatorHandler());
    list.add(new CustodianHandler());
    list.add(new DocumentationOfHandler());
    return list;
  }

  @Override
  protected PHMRDocument createNewDocument(CDAHeaderHandler headerHandler) {
    return new PHMRDocument(headerHandler.getDocumentId());
  }

  @Override
  protected List<CDAXmlHandler<PHMRDocument>> createSectionHandlers() {
    List<CDAXmlHandler<PHMRDocument>> list = new ArrayList<CDAXmlHandler<PHMRDocument>>();
    list.add(new SectionHandler());
    return list;
  }
}
