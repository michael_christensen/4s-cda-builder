package dk.s4.hl7.cda.convert.decode.general;

import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class QuestionOptionsPatternHandler extends BaseXmlHandler {
  public static final String OBSERVATION_BASE = "/ClinicalDocument/component/structuredBody/component/section/entry/organizer/component/observation/entryRelationship/observation";
  private String minimum;
  private String maximum;
  private String valueType;

  public QuestionOptionsPatternHandler() {
    addPath(OBSERVATION_BASE + "/value");
    addPath(OBSERVATION_BASE + "/value/low");
    addPath(OBSERVATION_BASE + "/value/high");
  }

  public String getMinimum() {
    return minimum;
  }

  public String getMaximum() {
    return maximum;
  }

  public String getValueType() {
    return valueType;
  }

  public boolean hasValues() {
    return minimum != null && maximum != null && valueType != null;
  }

  public void clear() {
    minimum = null;
    maximum = null;
    valueType = null;
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "value", "type")) {
      valueType = xmlElement.getAttributeValue("type");
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "low", "value")) {
      minimum = xmlElement.getAttributeValue("value");
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "high", "value")) {
      maximum = xmlElement.getAttributeValue("value");
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    // Ignore
  }

}
