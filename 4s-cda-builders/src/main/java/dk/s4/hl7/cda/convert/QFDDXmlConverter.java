package dk.s4.hl7.cda.convert;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.codes.IntervalType;
import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.convert.encode.BuildUtil;
import dk.s4.hl7.cda.convert.encode.ClinicalDocumentConverter;
import dk.s4.hl7.cda.convert.encode.pattern.ReferenceRangePattern;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.SectionInformation;
import dk.s4.hl7.cda.model.core.ClinicalDocument;
import dk.s4.hl7.cda.model.qfdd.QFDDAnalogSliderQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDDiscreteSliderQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDDocument;
import dk.s4.hl7.cda.model.qfdd.QFDDFeedback;
import dk.s4.hl7.cda.model.qfdd.QFDDHelpText;
import dk.s4.hl7.cda.model.qfdd.QFDDMultipleChoiceQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDNumericQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDPrecondition;
import dk.s4.hl7.cda.model.qfdd.QFDDQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDTextQuestion;
import dk.s4.hl7.util.xml.XmlPrettyPrinter;
import dk.s4.hl7.util.xml.XmlStreamBuilder;

/**
 * An implementation of the Converter that can build an XML Document object
 * following the Danish QFDD standard.
 *
 * To use you should:
 *
 * a) create a QFDDXmlCodec converter b) call the converter.encode(cda) where
 * cda is a QFDDDocument
 * 
 * @author Frank Jacobsen Systematic A/S
 */
public class QFDDXmlConverter extends ClinicalDocumentConverter<QFDDDocument> {

  public QFDDXmlConverter(XmlPrettyPrinter xmlPrettyPrinter) {
    super(xmlPrettyPrinter);
  }

  public QFDDXmlConverter() {
    this(new XmlPrettyPrinter());
  }

  protected void buildContext(ClinicalDocument document, XmlStreamBuilder xmlBuilder) throws IOException {
    // CONF-PHMR-15
    xmlBuilder.element("title").value(document.getTitle()).elementEnd();
    xmlBuilder.element("sdtc:statusCode").attribute("code", "NEW").elementShortEnd();

    // CONF-PHMR-16 / CONF-DK PHMR-17
    xmlBuilder
        .element("effectiveTime")
        .attribute("value", dateTimeformatter.format(document.getEffectiveTime()))
        .elementShortEnd();

    xmlBuilder
        .element("confidentialityCode")
        .attribute("code", "N")
        .attribute("codeSystem", HL7.CONFIDENTIALITY_OID)
        .elementShortEnd();

    // CONF-PHMR-17-20
    xmlBuilder.element("languageCode").attribute("code", document.getLanguageCode()).elementShortEnd();

    // CONF-DK PHMR-22
    if (isNotNullAndEmpty(document.getSetId())) {
      xmlBuilder
          .element("setId")
          .attribute("root", MedCom.MESSAGECODE_OID)
          .attribute("extension", document.getSetId())
          .elementShortEnd();
    }
    if (document.getVersionNumber() != null) {
      xmlBuilder.element("versionNumber").attribute("value", document.getVersionNumber().toString()).elementShortEnd();
    }
  }

  private static void generateTemplateIds(XmlStreamBuilder xmlBuilder, List<String> templateIds) throws IOException {
    for (String templateId : templateIds) {
      xmlBuilder.element("templateId").attribute("root", templateId).elementShortEnd();
    }
  }

  private void buildQFDDSections(List<Section<QFDDQuestion>> qfddSectionsList, XmlStreamBuilder xmlBuilder)
      throws IOException {
    for (Section<QFDDQuestion> qfddSection : qfddSectionsList) {
      List<QFDDQuestion> qfddQuestionList = qfddSection.getQuestionnaireEntities();

      if (!qfddQuestionList.isEmpty()) {
        xmlBuilder.element("component").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
        xmlBuilder.element("section").attribute("classCode", "DOCSECT").attribute("moodCode", "EVN");
        BuildUtil.buildTemplateIds(xmlBuilder, HL7.QFDD_SECTION_ROOT_OID);

        BuildUtil.buildCode(Loinc.QFD_CODE, Loinc.OID, null, "LOINC", xmlBuilder);

        xmlBuilder.element("title").value(qfddSection.getTitle()).elementEnd();
        // Text may be embedded HTML at this point
        xmlBuilder.element("text").valueNoEscaping(qfddSection.getText()).elementEnd();

        xmlBuilder
            .element("entry")
            .attribute("typeCode", "DRIV")
            .attribute("contextConductionInd", "true")
            .addElement(xmlBuilder.element("organizer").attribute("classCode", "BATTERY").attribute("moodCode", "EVN"));

        generateTemplateIds(xmlBuilder, Arrays.asList(HL7.QFDD_QUESTION_ORGANIZER));
        xmlBuilder.element("statusCode").attribute("code", "COMPLETED").elementShortEnd();
        buildQuestions(xmlBuilder, qfddQuestionList);
        xmlBuilder.elementEnd(); // End organizer
        xmlBuilder.elementEnd(); // End entry
        xmlBuilder.elementEnd(); // End section
        xmlBuilder.elementEnd(); // End component

      } else if (qfddSection.getSectionInformation() != null) {
        xmlBuilder.element("component").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
        xmlBuilder.element("section").attribute("classCode", "DOCSECT").attribute("moodCode", "EVN");
        BuildUtil.buildTemplateIds(xmlBuilder, HL7.QFDD_SECTION_ROOT_OID);

        xmlBuilder.element("title").value(qfddSection.getTitle()).elementEnd();
        // Text may be embedded HTML at this point
        xmlBuilder.element("text").valueNoEscaping(qfddSection.getText()).elementEnd();
        if (qfddSection.getLanguage() != null) {
          xmlBuilder.element("languageCode").attribute("code", qfddSection.getLanguage()).elementShortEnd();
        }
        xmlBuilder.elementEnd(); // End section
        xmlBuilder.elementEnd(); // End component
      }
    }
  }

  private void buildQuestions(XmlStreamBuilder xmlBuilder, List<QFDDQuestion> qfddQuestionList) throws IOException {
    int sequenceNumber = 0;
    for (QFDDQuestion qfddQuestion : qfddQuestionList) {
      buildOneQuestion(qfddQuestion, xmlBuilder, ++sequenceNumber);
    }
  } 

  private void buildCopyRight(SectionInformation copyRight, XmlStreamBuilder xmlBuilder) throws IOException {
    if (copyRight != null) {
      xmlBuilder.element("component").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
      xmlBuilder.element("section").attribute("classCode", "DOCSECT").attribute("moodCode", "EVN");
      BuildUtil.buildTemplateIds(xmlBuilder, HL7.QFDD_COPYRIGHT_SECTION_TEMPLATEID);
      xmlBuilder.element("title").value(copyRight.getTitle()).elementEnd();
      if (copyRight.getLanguage() != null) {
        xmlBuilder.element("languageCode").attribute("code", copyRight.getLanguage()).elementShortEnd();
      }

      xmlBuilder.element("entry").attribute("typeCode", "DRIV").attribute("contextConductionInd", "true");
      xmlBuilder.element("observation").attribute("classCode", "OBS").attribute("moodCode", "EVN");
      BuildUtil.buildTemplateIds(xmlBuilder, HL7.QFDD_COPYRIGHT_PATTERN_TEMPLATEID);

      BuildUtil.buildCode("COPY", Loinc.OID, "Code for Copyright", Loinc.DISPLAYNAME, xmlBuilder);

      xmlBuilder.element("value").attribute("xsi:type", "ST").value(copyRight.getText()).elementEnd();

      xmlBuilder.elementEnd(); // end entry
      xmlBuilder.elementEnd(); // end observation
      xmlBuilder.elementEnd(); // end component
      xmlBuilder.elementEnd(); // end section
    }

  }

  private static void buildOneQuestion(QFDDQuestion qfddQuestion, XmlStreamBuilder xmlBuilder, int sequenceNumber)
      throws IOException {

    xmlBuilder.element("component").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
    xmlBuilder.element("sequenceNumber").attribute("value", String.valueOf(sequenceNumber)).elementShortEnd();
    xmlBuilder.element("observation").attribute("classCode", "OBS").attribute("moodCode", "EVN");

    if (qfddQuestion instanceof QFDDNumericQuestion) {
      buildNumericQuestion(qfddQuestion, xmlBuilder);
    } else if (qfddQuestion instanceof QFDDDiscreteSliderQuestion) {
      buildDiscreteSliderQuestion(qfddQuestion, xmlBuilder);
    } else if (qfddQuestion instanceof QFDDAnalogSliderQuestion) {
      buildAnalogSliderQuestion(qfddQuestion, xmlBuilder);
    } else if (qfddQuestion instanceof QFDDTextQuestion) {
      buildTextQuestion(qfddQuestion, xmlBuilder);
    } else if (qfddQuestion instanceof QFDDMultipleChoiceQuestion) {
      buildMultipleChoiceQuestion(qfddQuestion, xmlBuilder);
    }

    externalReferencePattern.build(qfddQuestion.getReferences(), xmlBuilder);

    xmlBuilder.elementEnd(); // end observation
    xmlBuilder.elementEnd(); // end component

  }

  protected static void buildPrecondition(QFDDQuestion qfddQuestion, XmlStreamBuilder xmlBuilder) throws IOException {
    if (qfddQuestion.getPreconditions() != null && qfddQuestion.getPreconditions().size() > 0) {
      for (QFDDPrecondition qfddPrecondition : qfddQuestion.getPreconditions()) {
        buildPrecondition(qfddPrecondition, xmlBuilder);
      }
    }
  }

  protected static void buildFeedback(QFDDQuestion qfddQuestion, XmlStreamBuilder xmlBuilder) throws IOException {
    if (qfddQuestion.getFeedback() != null) {
      xmlBuilder.element("entryRelationship").attribute("typeCode", "REFR").attribute("contextConductionInd", "true");
      buildQuestionFeedback(qfddQuestion.getFeedback(), xmlBuilder);
      xmlBuilder.elementEnd();
    }
  }

  private static void buildQuestionHelpText(QFDDHelpText qfddHelpText, XmlStreamBuilder xmlBuilder) throws IOException {
    if (qfddHelpText == null)
      return;

    xmlBuilder.element("entryRelationship").attribute("typeCode", "SUBJ").attribute("contextConductionInd", "true");
    xmlBuilder.element("observation").attribute("classCode", "OBS").attribute("moodCode", "EVN");

    generateTemplateIds(xmlBuilder, Arrays.asList(HL7.QFDD_QUESTION_HELP_TEXT_PATTERN_OID));
    BuildUtil.buildCode(Loinc.HELPTEXT_CODE, Loinc.OID, Loinc.HELPTEXT_DISPLAYNAME, Loinc.DISPLAYNAME, xmlBuilder);

    xmlBuilder.element("value").attribute("xsi:type", "ST");
    if (qfddHelpText.getLanguage() != null) {
      xmlBuilder.attribute("language", qfddHelpText.getLanguage());
    }
    xmlBuilder.value(qfddHelpText.getHelpText()).elementEnd();

    xmlBuilder.elementEnd();// end Help text
    xmlBuilder.elementEnd();// end entryRelationship
  }

  private static void buildQuestionFeedback(QFDDFeedback qfddFeedback, XmlStreamBuilder xmlBuilder) throws IOException {
    if (qfddFeedback == null)
      return;

    xmlBuilder.element("observation").attribute("classCode", "OBS").attribute("moodCode", "DEF");
    generateTemplateIds(xmlBuilder, Arrays.asList(HL7.QFDD_QUESTION_FEEDBACK_PATTERN_OID));
    BuildUtil.buildCode(Loinc.FEEDBACK_CODE, Loinc.OID, Loinc.FEEDBACK_DISPLAYNAME, Loinc.DISPLAYNAME, xmlBuilder);
    xmlBuilder.element("value").attribute("xsi:type", "ST");
    if (qfddFeedback.getLanguage() != null) {
      xmlBuilder.attribute("language", qfddFeedback.getLanguage());
    }
    xmlBuilder.value(qfddFeedback.getFeedBackText()).elementEnd();
    for (QFDDPrecondition qfddPrecondition : qfddFeedback.getQFDDPreconditionList()) {
      buildPrecondition(qfddPrecondition, xmlBuilder);
    }
    xmlBuilder.elementEnd();

  }

  private static void buildPrecondition(QFDDPrecondition qfddPrecondition, XmlStreamBuilder xmlBuilder)
      throws IOException {
    if (qfddPrecondition.getValueType() != null) {
      xmlBuilder.element("precondition").attribute("typeCode", "PRCN");
      generateTemplateIds(xmlBuilder, Arrays.asList(HL7.QFDD_PRECONDITION_PATTERN_OID));
      xmlBuilder.element("criterion").attribute("classCode", "OBS").attribute("moodCode", "EVN.CRT");
      generateTemplateIds(xmlBuilder, Arrays.asList(HL7.QFDD_CRITERION_PATTERN_OID));
      BuildUtil.buildCode(qfddPrecondition.getCodeValue(), xmlBuilder);

      // This is a min/max precondition
      if (qfddPrecondition.getMinimum() != null && qfddPrecondition.getMinimum().trim().length() > 0) {
        xmlBuilder.element("value").attribute("xsi:type", qfddPrecondition.getValueType());
        xmlBuilder.element("low").attribute("value", qfddPrecondition.getMinimum()).elementShortEnd();
        xmlBuilder.element("high").attribute("value", qfddPrecondition.getMaximum()).elementShortEnd();
        xmlBuilder.elementEnd(); // end value
      } else if (qfddPrecondition.getAnswer() != null) {
        CodedValue answer = qfddPrecondition.getAnswer();
        xmlBuilder.element("value").attribute("xsi:type", qfddPrecondition.getValueType());
        buildValueCodedValue(xmlBuilder, answer);
        xmlBuilder.elementShortEnd();
      }

      xmlBuilder.elementEnd(); // end criterion
      xmlBuilder.elementEnd(); // end precondition
    }
  }

  private static void buildValueCodedValue(XmlStreamBuilder xmlBuilder, CodedValue answer) throws IOException {
    if (answer.getCode() != null) {
      xmlBuilder.attribute("code", answer.getCode());
    }
    if (answer.getCodeSystem() != null) {
      xmlBuilder.attribute("codeSystem", answer.getCodeSystem());
    }
    if (answer.getDisplayName() != null) {
      xmlBuilder.attribute("displayName", answer.getDisplayName());
    }
    if (answer.getCodeSystemName() != null) {
      xmlBuilder.attribute("codeSystemName", answer.getCodeSystemName());
    }
  }

  private static void buildMultipleChoiceQuestion(QFDDQuestion qfddQuestion, XmlStreamBuilder xmlBuilder)
      throws IOException {

    QFDDMultipleChoiceQuestion qfddMultipleChoiceQuestion = (QFDDMultipleChoiceQuestion) qfddQuestion;
    generateTemplateIds(xmlBuilder, Arrays.asList(HL7.QFDD_MULTIPLE_CHOICE_QUESTION_PATTERN_OID));

    BuildUtil.buildId(qfddQuestion.getId(), xmlBuilder);
    BuildUtil.buildQuestionnaireEntityCode(qfddQuestion, xmlBuilder);

    for (CodedValue codeValue : qfddMultipleChoiceQuestion.getAnswerOptionList()) {
      xmlBuilder
          .element("value")
          .attribute("xsi:type", "CE")
          .attribute("code", codeValue.getCode())
          .attribute("codeSystem", codeValue.getCodeSystem())
          .attribute("displayName", codeValue.getDisplayName())
          .attribute("codeSystemName", codeValue.getCodeSystemName())
          .elementShortEnd();
    }

    xmlBuilder.element("entryRelationship").attribute("typeCode", "SUBJ").attribute("contextConductionInd", "true");
    xmlBuilder.element("observation").attribute("classCode", "OBS").attribute("moodCode", "EVN");
    generateTemplateIds(xmlBuilder, Arrays.asList(HL7.QUESTION_OPTIONS_PATTERN_OBSERVATION_OID));

    BuildUtil.buildCode(Loinc.SECTION_QRD_CODE, Loinc.OID, Loinc.DISPLAYNAME, xmlBuilder);

    xmlBuilder.element("value").attribute("xsi:type", "IVL_INT");
    xmlBuilder.element("low").attribute("value", qfddMultipleChoiceQuestion.getMinimum()).elementShortEnd();
    xmlBuilder.element("high").attribute("value", qfddMultipleChoiceQuestion.getMaximum()).elementShortEnd();

    xmlBuilder.elementEnd(); // end value
    xmlBuilder.elementEnd(); // end observation
    xmlBuilder.elementEnd(); // end entryRelationship

    buildFeedback(qfddQuestion, xmlBuilder);
    buildQuestionHelpText(qfddQuestion.getHelpText(), xmlBuilder);
    buildPrecondition(qfddQuestion, xmlBuilder);
  }

  private static void buildTextQuestion(QFDDQuestion qfddQuestion, XmlStreamBuilder xmlBuilder) throws IOException {
    generateTemplateIds(xmlBuilder, Arrays.asList(HL7.QFDD_TEXT_QUESTION_PATTERN_OID));
    BuildUtil.buildId(qfddQuestion.getId(), xmlBuilder);
    BuildUtil.buildQuestionnaireEntityCode(qfddQuestion, xmlBuilder);
    buildFeedback(qfddQuestion, xmlBuilder);
    buildQuestionHelpText(qfddQuestion.getHelpText(), xmlBuilder);
    buildPrecondition(qfddQuestion, xmlBuilder);
  }

  private static void buildAnalogSliderQuestion(QFDDQuestion qfddQuestion, XmlStreamBuilder xmlBuilder)
      throws IOException {
    QFDDAnalogSliderQuestion qfddAnalogSliderQuestion = (QFDDAnalogSliderQuestion) qfddQuestion;
    generateTemplateIds(xmlBuilder, Arrays.asList(HL7.QFDD_NUMERIC_PATTERN_OID, HL7.QFDD_ANALOG_SLIDER_PATTERN_OID));

    BuildUtil.buildId(qfddQuestion.getId(), xmlBuilder);
    BuildUtil.buildQuestionnaireEntityCode(qfddQuestion, xmlBuilder);

    buildFeedback(qfddQuestion, xmlBuilder);
    buildQuestionHelpText(qfddQuestion.getHelpText(), xmlBuilder);
    buildPrecondition(qfddQuestion, xmlBuilder);

    xmlBuilder.element("referenceRange").attribute("typeCode", "REFV");
    xmlBuilder.element("observationRange");

    xmlBuilder
        .element("value")
        .attribute("xsi:type", IntervalType.GLIST_PQ.name())
        .attribute("denominator", qfddAnalogSliderQuestion.getMaximum());

    xmlBuilder.element("head").attribute("value", qfddAnalogSliderQuestion.getMinimum()).elementShortEnd();
    xmlBuilder.element("increment").attribute("value", qfddAnalogSliderQuestion.getIncrement()).elementShortEnd();
    xmlBuilder.elementEnd(); // end value
    xmlBuilder.elementEnd(); // end observationRange
    xmlBuilder.elementEnd(); // end referenceRange
  }

  private static void buildDiscreteSliderQuestion(QFDDQuestion qfddQuestion, XmlStreamBuilder xmlBuilder)
      throws IOException {
    QFDDDiscreteSliderQuestion qfddDiscreteSliderQuestion = (QFDDDiscreteSliderQuestion) qfddQuestion;
    generateTemplateIds(xmlBuilder,
        Arrays.asList(HL7.QFDD_MULTIPLE_CHOICE_QUESTION_PATTERN_OID, HL7.QFDD_DISCRETE_SLIDER_PATTERN_OID));

    BuildUtil.buildId(qfddQuestion.getId(), xmlBuilder);
    BuildUtil.buildQuestionnaireEntityCode(qfddQuestion, xmlBuilder);

    for (CodedValue codeValue : qfddDiscreteSliderQuestion.getAnswerOptionList()) {
      xmlBuilder
          .element("value")
          .attribute("xsi:type", "CE")
          .attribute("code", codeValue.getCode())
          .attribute("codeSystem", codeValue.getCodeSystem())
          .attribute("displayName", codeValue.getDisplayName())
          .attribute("codeSystemName", codeValue.getCodeSystemName())
          .elementShortEnd();
    }

    xmlBuilder.element("entryRelationship").attribute("typeCode", "SUBJ").attribute("contextConductionInd", "true");
    xmlBuilder.element("observation").attribute("classCode", "OBS").attribute("moodCode", "EVN");
    generateTemplateIds(xmlBuilder, Arrays.asList(HL7.QUESTION_OPTIONS_PATTERN_OBSERVATION_OID));

    BuildUtil.buildCode(Loinc.SECTION_QRD_CODE, Loinc.OID, Loinc.DISPLAYNAME, xmlBuilder);

    xmlBuilder.element("value").attribute("xsi:type", "IVL_INT");
    xmlBuilder.element("low").attribute("value", qfddDiscreteSliderQuestion.getMinimum()).elementShortEnd();
    xmlBuilder.element("high").attribute("value", qfddDiscreteSliderQuestion.getMaximum()).elementShortEnd();
    xmlBuilder.elementEnd(); // end value
    xmlBuilder.elementEnd(); // end observation
    xmlBuilder.elementEnd(); // end entryRelationship
    buildFeedback(qfddQuestion, xmlBuilder);
    buildQuestionHelpText(qfddQuestion.getHelpText(), xmlBuilder);
    buildPrecondition(qfddQuestion, xmlBuilder);
  }

  private static void buildNumericQuestion(QFDDQuestion qfddQuestion, XmlStreamBuilder xmlBuilder) throws IOException {
    QFDDNumericQuestion qfddNumericQuestion = (QFDDNumericQuestion) qfddQuestion;
    generateTemplateIds(xmlBuilder, Arrays.asList(HL7.QFDD_NUMERIC_PATTERN_OID));
    BuildUtil.buildId(qfddQuestion.getId(), xmlBuilder);
    BuildUtil.buildQuestionnaireEntityCode(qfddQuestion, xmlBuilder);
    buildFeedback(qfddQuestion, xmlBuilder);
    buildQuestionHelpText(qfddQuestion.getHelpText(), xmlBuilder);
    buildPrecondition(qfddQuestion, xmlBuilder);
    new ReferenceRangePattern().build(qfddNumericQuestion, xmlBuilder);
  }

  public String convert(QFDDDocument source) {
    int nofObservations = source.getInformationRecipients().size() + source.getSections().size();
    for (Section<QFDDQuestion> section : source.getSections()) {
      nofObservations += section.getQuestionnaireEntities().size();
    }
    StringBuilder builder = new StringBuilder(8000 + (nofObservations * 1500));
    serialize(source, builder);
    return builder.toString();
  }

  @Override
  protected void buildPatientSection(ClinicalDocument document, XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.element("recordTarget").attribute("typeCode", "RCT").attribute("contextControlCode", "OP");
    xmlBuilder.element("patientRole").attribute("classCode", "PAT");
    xmlBuilder.element("id").attribute("nullFlavor", "NI").elementShortEnd();
    xmlBuilder.elementEnd(); // end recordTarget
    xmlBuilder.elementEnd(); // end patientRole
  }

  @Override
  protected void buildDocumentationOf(QFDDDocument document, XmlStreamBuilder xmlBuilder) throws IOException {
    // NOT used.
  }

  @Override
  protected void buildDataEnterer(ClinicalDocument document, XmlStreamBuilder xmlBuilder) throws IOException {
    // NOT USED
  }

  @Override
  protected void buildInformationRecipient(ClinicalDocument document, XmlStreamBuilder xmlBuilder) throws IOException {
    // NOT USED
  }

  @Override
  protected void buildParticipants(ClinicalDocument document, XmlStreamBuilder xmlBuilder) throws IOException {
    // NOT USED
  }

  @Override
  protected void buildLegalAuthenticatorSection(ClinicalDocument document, XmlStreamBuilder xmlBuilder)
      throws IOException {
    // Not used
  }

  @Override
  protected void buildBody(QFDDDocument document, XmlStreamBuilder xmlBuilder) throws IOException {
    this.buildStructuredBodySectionStart(xmlBuilder);
    this.buildQFDDSections(document.getSections(), xmlBuilder);
    this.buildCopyRight(document.getCopyRight(), xmlBuilder);
    this.buildStructuredBodySectionEnd(xmlBuilder);
  }
}
