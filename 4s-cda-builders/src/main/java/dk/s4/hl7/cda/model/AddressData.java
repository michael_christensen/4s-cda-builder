package dk.s4.hl7.cda.model;

import java.util.*;

/** Immutable object to contain an address.
 *
 * @author Henrik Baerbak Christensen, Aarhus University
 */
public final class AddressData {

  /** Address data use enumeration.
   */
  public enum Use {
    /** Home address. */
    HomeAddress,
    /** Work place address. */
    WorkPlace,
  }

  private String[] streetLines;
  private String postalCode;
  private String city;
  private String country;
  private Use addressUse;

  /** "Effective Java" Builder for constructing addresses.
   *
   * @author Henrik Baerbak Christensen, Aarhus University
   *
   */
  public static class AddressBuilder {
    private List<String> streetLinesTemporary;
    private String[] streetLines = null;
    private String postalCode;
    private String city;
    private String country;
    private Use addressUse;

    public AddressBuilder() {
      streetLinesTemporary = new ArrayList<String>();
    }

    public AddressBuilder(String postalcode, String city) {
      streetLinesTemporary = new ArrayList<String>();
      this.postalCode = postalcode;
      this.city = city;
    }

    public AddressBuilder addAddressLine(String addressLine) {
      streetLinesTemporary.add(addressLine);
      return this;
    }

    public AddressBuilder setPostalCode(String postalCode) {
      this.postalCode = postalCode;
      return this;
    }

    public AddressBuilder setCity(String city) {
      this.city = city;
      return this;
    }

    public AddressBuilder setCountry(String country) {
      this.country = country;
      return this;
    }

    public AddressBuilder setUse(Use theUse) {
      this.addressUse = theUse;
      return this;
    }

    public AddressData build() {
      if (streetLinesTemporary.size() > 0) {
        streetLines = new String[streetLinesTemporary.size()];
        streetLinesTemporary.toArray(streetLines);
      } else {
        streetLines = new String[0];
      }

      return new AddressData(this);
    }
  }

  private AddressData(AddressBuilder builder) {
    streetLines = builder.streetLines;
    postalCode = builder.postalCode;
    city = builder.city;
    country = builder.country;
    addressUse = builder.addressUse;
  }

  /** Get the list of streets.
   * @return list of streets.
   */
  public String[] getStreet() {
    return streetLines;
  }

  /** Get the postal code.
   * @return postal code.
   */
  public String getPostalCode() {
    return postalCode;
  }

  /** Get the city name.
   * @return city name.
   */
  public String getCity() {
    return city;
  }

  /** Get the country name.
   * @return country name.
   */
  public String getCountry() {
    return country;
  }

  /** Get the address use.
   * @return address use.
   */
  public Use getAddressUse() {
    return addressUse;
  }

  public String toString() {
    String result = "Address: ";

    result += getPostalCode() + " " + getCity() + " ";

    if (streetLines != null) {
      result += "Street: [";
      for (String line : getStreet()) {
        result += line + ",";
      }
      result += "] ";
    }

    result += country + " ";

    result += "(Use: " + addressUse + ")";

    return result;
  }
}
