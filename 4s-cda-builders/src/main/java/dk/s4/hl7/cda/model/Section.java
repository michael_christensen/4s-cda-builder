package dk.s4.hl7.cda.model;

import java.util.ArrayList;
import java.util.List;

public class Section<E extends QuestionnaireEntity> {
  private SectionInformation sectionInformation;
  private List<E> questionnaireEntities;

  public Section(String title, String text) {
    this(title, text, null);
  }

  public Section(String title, String text, String language) {
    this.sectionInformation = new SectionInformation(title, text, language);
    this.questionnaireEntities = new ArrayList<E>();
  }

  public Section(SectionInformation sectionInformation) {
    this.sectionInformation = sectionInformation;
    this.questionnaireEntities = new ArrayList<E>();
  }

  public String getTitle() {
    if (sectionInformation != null) {
      return sectionInformation.getTitle();
    }
    return null;
  }

  public String getText() {
    if (sectionInformation != null) {
      return sectionInformation.getText();
    }
    return null;
  }

  public String getLanguage() {
    if (sectionInformation != null) {
      return sectionInformation.getLanguage();
    }
    return null;
  }

  public SectionInformation getSectionInformation() {
    return sectionInformation;
  }

  public void addQuestionnaireEntity(E questionnaireEntity) {
    questionnaireEntities.add(questionnaireEntity);
  }

  public List<E> getQuestionnaireEntities() {
    return questionnaireEntities;
  }

  @Override
  public String toString() {
    if (sectionInformation != null) {
      return "Section [title=" + getText() + ", text=" + getText() + ", questionnaireEntities=" + questionnaireEntities
          + "]";
    }
    return "Section [title='', text='', questionnaireEntities=" + questionnaireEntities + "]";
  }
}
