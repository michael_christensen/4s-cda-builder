package dk.s4.hl7.cda.model.qfdd;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.model.CodedValue;

public final class QFDDDiscreteSliderQuestion extends QFDDQuestion {
  private List<CodedValue> answerOptionList = new ArrayList<CodedValue>();
  private String minimum; // head value

  /**
   * "Effective Java" Builder for constructing QFDDDiscreteSliderResponse.
   *
   * @author Frank Jacobsen, Systematic
   *
   */
  public static class QFDDDiscreteSliderQuestionBuilder extends
      QFDDQuestion.BaseQFDDQuestionBuilder<QFDDDiscreteSliderQuestion, QFDDDiscreteSliderQuestionBuilder> {

    private List<CodedValue> answerOptionList = new ArrayList<CodedValue>();

    public QFDDDiscreteSliderQuestionBuilder() {
    }

    private String minimum; // head value

    public QFDDDiscreteSliderQuestionBuilder setMinimum(String minimum) {
      this.minimum = minimum;
      return this;
    }

    public void addAnswerOption(String code, String codeSystem, String displayName, String codeSystemName) {
      answerOptionList.add(new CodedValue(code, codeSystem, displayName, codeSystemName));
    }

    @Override
    public QFDDDiscreteSliderQuestion build() {
      return new QFDDDiscreteSliderQuestion(this);
    }

    @Override
    public QFDDDiscreteSliderQuestionBuilder getThis() {
      return this;
    }
  }

  private QFDDDiscreteSliderQuestion(QFDDDiscreteSliderQuestionBuilder builder) {
    super(builder);
    minimum = builder.minimum;
    answerOptionList = builder.answerOptionList;
  }

  public void addAnswerOption(String code, String codeSystem, String displayName, String codeSystemName) {
    answerOptionList.add(new CodedValue(code, codeSystem, displayName, codeSystemName));
  }

  public List<CodedValue> getAnswerOptionList() {
    return answerOptionList;
  }

  public String getMinimum() {
    return minimum;
  }

  public String getMaximum() {
    return "1";
  }
}
