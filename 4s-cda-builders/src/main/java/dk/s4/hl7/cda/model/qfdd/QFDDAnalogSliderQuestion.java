package dk.s4.hl7.cda.model.qfdd;


public class QFDDAnalogSliderQuestion extends QFDDQuestion {
  private String minimum; // head value
  private String maximum; // denominator
  private String increment;

  /**
   * "Effective Java" Builder for constructing QFDDAnalogSliderResponse.
   *
   * @author Frank Jacobsen, Systematic
   *
   */
  public static class QFDDAnalogSliderQuestionBuilder extends
      QFDDQuestion.BaseQFDDQuestionBuilder<QFDDAnalogSliderQuestion, QFDDAnalogSliderQuestionBuilder> {

    private String minimum; // head value
    private String maximum; // denominator
    private String increment;

    public QFDDAnalogSliderQuestionBuilder setMinimum(String minimum) {
      this.minimum = minimum;
      return this;
    }

    public QFDDAnalogSliderQuestionBuilder setMaximum(String maximum) {
      this.maximum = maximum;
      return this;
    }

    public QFDDAnalogSliderQuestionBuilder setIncrement(String increment) {
      this.increment = increment;
      return this;
    }

    @Override
    public QFDDAnalogSliderQuestion build() {
      return new QFDDAnalogSliderQuestion(this);
    }

    @Override
    public QFDDAnalogSliderQuestionBuilder getThis() {
      return this;
    }
  }

  private QFDDAnalogSliderQuestion(QFDDAnalogSliderQuestionBuilder builder) {
    super(builder);
    minimum = builder.minimum;
    maximum = builder.maximum;
    increment = builder.increment;
  }

  public String getMinimum() {
    return minimum;
  }

  public String getMaximum() {
    return maximum;
  }

  public String getIncrement() {
    return increment;
  }

}
