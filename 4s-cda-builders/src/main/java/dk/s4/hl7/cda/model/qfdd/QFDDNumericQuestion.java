package dk.s4.hl7.cda.model.qfdd;

import dk.s4.hl7.cda.codes.IntervalType;

public class QFDDNumericQuestion extends QFDDQuestion {
  private String minimum; // low
  private String maximum; // high
  private IntervalType intervalType;

  /**
   * "Effective Java" Builder for constructing QFDDNumericQuestion.
   *
   * @author Frank Jacobsen, Systematic
   *
   */
  public static class QFDDNumericQuestionBuilder extends
      QFDDQuestion.BaseQFDDQuestionBuilder<QFDDNumericQuestion, QFDDNumericQuestionBuilder> {

    public QFDDNumericQuestionBuilder() {
    }

    private String minimum; // head value
    private String maximum; // denominator
    private IntervalType intervalType;

    public QFDDNumericQuestionBuilder setInterval(String minimum, String maximum, IntervalType intervalType) {
      this.maximum = maximum;
      this.minimum = minimum;
      this.intervalType = intervalType;
      replaceCommaWithDots();
      return this;
    }

    private void replaceCommaWithDots() {
      if (intervalType == IntervalType.IVL_REAL) {
        minimum = minimum.replace(',', '.');
        maximum = maximum.replace(',', '.');
      }
    }

    public QFDDNumericQuestion build() {
      return new QFDDNumericQuestion(this);
    }

    @Override
    public QFDDNumericQuestionBuilder getThis() {
      return this;
    }
  }

  private QFDDNumericQuestion(QFDDNumericQuestionBuilder builder) {
    super(builder);
    minimum = builder.minimum;
    maximum = builder.maximum;
    intervalType = builder.intervalType;
  }

  public String getMinimum() {
    return minimum;
  }

  public String getMaximum() {
    return maximum;
  }

  public IntervalType getIntervalType() {
    return intervalType;
  }
  
  public String getIntervalTypeAsString() {
    if (intervalType != null) {
      return intervalType.name();
    }
    return null;
  }
}
