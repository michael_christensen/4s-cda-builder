package dk.s4.hl7.cda.model;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.model.util.ModelUtil;

public class QuestionnaireEntity {
  private String question;
  private CodedValue codeValue;
  private ID id;
  private List<Reference> references;

  public static abstract class QuestionnaireEntityBuilder<R extends QuestionnaireEntity, T extends QuestionnaireEntityBuilder<R, T>> {
    protected String question;
    protected CodedValue codeValue;
    protected ID id;
    protected List<Reference> references;

    public QuestionnaireEntityBuilder() {
      references = new ArrayList<Reference>();
    }

    public T setQuestion(String question) {
      this.question = question;
      return getThis();
    }

    public T setCodeValue(CodedValue codeValue) {
      this.codeValue = codeValue;
      return getThis();
    }

    public T setId(ID id) {
      this.id = id;
      return getThis();
    }

    public T addReference(Reference reference) {
      if (reference != null) {
        references.add(reference);
      }
      return getThis();
    }

    /**
     * This fixes the need for type casting
     * 
     * @return T
     */
    public abstract T getThis();

    public R build() {
      ModelUtil.checkNull(question, "The question is mandatory");
      ModelUtil.checkNull(codeValue, "The code is mandatory");
      ModelUtil.checkNull(id, "The response Id is mandatory");
      return null;
    }

  }

  protected QuestionnaireEntity(QuestionnaireEntityBuilder<?, ?> builder) {
    this.question = builder.question;
    this.codeValue = builder.codeValue;
    this.id = builder.id;
    this.references = builder.references;
  }

  public String getQuestion() {
    return question;
  }

  public CodedValue getCode() {
    if (codeValue == null) {
      return null;
    }
    return codeValue;
  }

  public ID getId() {
    return id;
  }

  public void addReference(Reference reference) {
    if (reference != null) {
      references.add(reference);
    }
  }

  public List<Reference> getReferences() {
    return references;
  }
}
