package dk.s4.hl7.cda.convert.decode.header;

import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.convert.decode.general.AddressHandler;
import dk.s4.hl7.cda.convert.decode.general.IdHandler;
import dk.s4.hl7.cda.convert.decode.general.PersonHandler;
import dk.s4.hl7.cda.convert.decode.general.TelecomHandler;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.Patient.PatientBuilder;
import dk.s4.hl7.cda.model.core.ClinicalDocument;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class PatientHandler implements CDAXmlHandler<ClinicalDocument> {
  private static final String PATIENT_PATH = "/ClinicalDocument/recordTarget/patientRole";
  private IdHandler idHandler;
  private AddressHandler addressHandler;
  private TelecomHandler telecomHandler;
  private PersonHandler personHandler;
  private boolean hasPatient;

  public PatientHandler() {
    this.personHandler = new PersonHandler(PATIENT_PATH, "patient");
    this.addressHandler = new AddressHandler(PATIENT_PATH, Use.HomeAddress);
    this.idHandler = new IdHandler(PATIENT_PATH);
    this.telecomHandler = new TelecomHandler(PATIENT_PATH);
    this.hasPatient = false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "patientRole")) {
      hasPatient = true;
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    // Ignore
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    xmlMapping.remove(PATIENT_PATH);
    idHandler.removeHandlerFromMap(xmlMapping);
    addressHandler.removeHandlerFromMap(xmlMapping);
    telecomHandler.removeHandlerFromMap(xmlMapping);
    personHandler.removeHandlerFromMap(xmlMapping);
  }

  public void addHandlerToMap(XmlMapping xmlMapping) {
    xmlMapping.add(PATIENT_PATH, this);
    idHandler.addHandlerToMap(xmlMapping);
    addressHandler.addHandlerToMap(xmlMapping);
    telecomHandler.addHandlerToMap(xmlMapping);
    personHandler.addHandlerToMap(xmlMapping);
  }

  public void addDataToDocument(ClinicalDocument clinicalDocument) {
    if (hasPatient) {
      PatientBuilder patientBuilder = new PatientBuilder();
      if (idHandler.getId() != null) {
        patientBuilder.setPersonID(idHandler.getId().getExtension());
      }
      personHandler.addPatientInformation(patientBuilder);
      patientBuilder.setAddress(addressHandler.getAddress());
      patientBuilder.addTelecoms(telecomHandler.getTelecoms());
      clinicalDocument.setPatient(patientBuilder.build());
    }
  }

  @Override
  public boolean includeChildren() {
    return false;
  }
}