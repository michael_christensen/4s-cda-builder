package dk.s4.hl7.util.xml;

import java.io.IOException;

public class XmlPrettyPrinter {
  public static final String DEFAULT_INDENTATION = "  ";
  public static final String DEFAULT_NEWLINE = String.format("%n");
  public static final int DEFAULT_MAX_EXPECTED_XML_DEPTH = 50;

  private String[] indentations;
  private final String newline;

  public XmlPrettyPrinter(int maxXmlDepth, String indentation, String newline) {
    createIndentationArray(maxXmlDepth, indentation);
    this.newline = newline;
  }

  public XmlPrettyPrinter(int maxXmlDepth) {
    this(maxXmlDepth, DEFAULT_INDENTATION, DEFAULT_NEWLINE);
  }

  public XmlPrettyPrinter() {
    this(DEFAULT_MAX_EXPECTED_XML_DEPTH, DEFAULT_INDENTATION, DEFAULT_NEWLINE);
  }

  /**
   * Creates a list of indentations for xml depth 0 to maxXmlDepth.
   * 
   * e.g.:
   * |Xml Depth |Indentation
   * -----------------------
   * |0         |""
   * |1         |"\t"
   * |2         |"\t\t"
   * |3         |"\t\t\t"
   * ....
   * -----------------------
   * This simplifies the pretty printing process to a list lookup instead
   * of creating the correct number of indentations each time. 
   * 
   * @param maxXmlDepth
   * @param indentation
   */
  private void createIndentationArray(int maxXmlDepth, String indentation) {
    maxXmlDepth = maxXmlDepth + 1; // Add no indentation special case
    indentations = new String[maxXmlDepth];
    indentations[0] = "";
    StringBuilder builder = new StringBuilder(128);
    for (int i = 1; i < maxXmlDepth; i++) {
      builder.setLength(0); // reset
      for (int j = 0; j < i; j++) {
        builder.append(indentation);
      }
      indentations[i] = builder.toString();
    }
  }

  public void appendIndentation(Appendable appendable, int depth) throws IOException {
    if (depth > 0 && indentations.length > depth) {
      appendable.append(indentations[depth]);
    }
  }

  public void appendNewline(Appendable appendable) throws IOException {
    appendable.append(newline);
  }
}
