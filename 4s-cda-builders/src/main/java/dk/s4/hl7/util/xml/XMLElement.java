package dk.s4.hl7.util.xml;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.lang3.StringEscapeUtils;

public class XMLElement {
  private XmlStreamWrapper xmlStreamWrapper;
  private String xpath;
  private String elementValue;
  private XmlRawTextReader rawTextReader;

  public XMLElement(XmlStreamWrapper xmlStreamWrapper, XmlRawTextReader rawTextReader) {
    this.xmlStreamWrapper = xmlStreamWrapper;
    this.rawTextReader = rawTextReader;
  }
  
  public XMLElement(XmlStreamWrapper xmlStreamWrapper) {
    this(xmlStreamWrapper, null);
  }

  public void setXPath(String xpath) {
    this.xpath = xpath;
    elementValue = null;
  }

  public String getXPath() {
    return xpath;
  }

  public String getElementName() {
    return xmlStreamWrapper.getLocalName();
  }

  public String getAttributeValue(String attributeName) {
    if (xmlStreamWrapper.isCurrentStartElement()) {
      return StringEscapeUtils.unescapeXml(xmlStreamWrapper.getAttributeValue(null, attributeName));
    }
    return null;
  }

  public void startCaptureRawText() {
    if (rawTextReader != null) {
      rawTextReader.continuesCapture();
      xmlStreamWrapper.skipChildrenOnNext();
    }
  }

  /**
   * NB: Only use this after all attributes have been read - Calling this will
   * move the XML parser forward
   * 
   */
  public String getElementValue() {
    if (elementValue == null) {
      if (xmlStreamWrapper.isCurrentEndElement()) {
        int[] startEndIndices = xmlStreamWrapper.getCurrentStartEndIndexPair();
        elementValue = rawTextReader.readRawText(startEndIndices[0], startEndIndices[1]);
      } else {
        try {
          elementValue = StringEscapeUtils.unescapeXml(xmlStreamWrapper.getElementText());
        } catch (XMLStreamException e) {
          // Element has children and does not have a value/text
          return null;
        }
      }
    }
    return elementValue;
  }
}
