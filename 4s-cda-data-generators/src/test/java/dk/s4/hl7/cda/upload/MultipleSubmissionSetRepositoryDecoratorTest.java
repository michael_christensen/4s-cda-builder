package dk.s4.hl7.cda.upload;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.net4care.xdsconnector.IRepositoryConnector;
import org.net4care.xdsconnector.Utilities.CodedValue;
import org.net4care.xdsconnector.service.RegistryResponseType;
import org.net4care.xdsconnector.service.RetrieveDocumentSetResponseType;
import org.w3c.dom.Document;

public class MultipleSubmissionSetRepositoryDecoratorTest {
  private static final String PATIENT2_CPR = "208021923";
  private static final String PATIENT1_CPR = "0101062112";
  private static final String PHMR_TYPE = "53576-5";
  private static final String QRD_TYPE = "74465-6";
  
  private static String PHMR_0101062112_0;
  private static String PHMR_0101062112_1;
  private static String PHMR_0208021923_0;
  private static String PHMR_0208021923_1;
  
  private static String QRD_0101062112_0;
  private static String QRD_0101062112_1;
  private static String QRD_0208021923_0;
  private static String QRD_0208021923_1;
  
  @BeforeClass
  public static void startup() throws Exception {
    PHMR_0101062112_0 = FileLoaderUtil.loadFile("testdocuments/PhmrDataFileFor0101062112_0.xml");
    PHMR_0101062112_1 = FileLoaderUtil.loadFile("testdocuments/PhmrDataFileFor0101062112_1.xml");
    
    PHMR_0208021923_0 = FileLoaderUtil.loadFile("testdocuments/PhmrDataFileFor0208021923_0.xml");
    PHMR_0208021923_1 = FileLoaderUtil.loadFile("testdocuments/PhmrDataFileFor0208021923_1.xml");
    
    QRD_0101062112_0 = FileLoaderUtil.loadFile("testdocuments/QrdDataFileFor0101062112_0.xml");
    QRD_0101062112_1 = FileLoaderUtil.loadFile("testdocuments/QrdDataFileFor0101062112_1.xml");
    
    QRD_0208021923_0 = FileLoaderUtil.loadFile("testdocuments/QrdDataFileFor0208021923_0.xml");
    QRD_0208021923_1 = FileLoaderUtil.loadFile("testdocuments/QrdDataFileFor0208021923_1.xml");
  }
  
  @Test
  public void successTestOnMixedPatientsAndDocumentTypes() throws Exception {
    // Arrange
    List<String> alldocuments = new ArrayList<String>();
    List<String> patient1PHMRdocuments = Arrays.asList(PHMR_0101062112_0, PHMR_0101062112_1);
    List<String> patient2PHMRdocuments = Arrays.asList(PHMR_0208021923_0, PHMR_0208021923_1);
    List<String> patient1QRDdocuments = Arrays.asList(QRD_0101062112_0, QRD_0101062112_1);
    List<String> patient2QRDdocuments = Arrays.asList(QRD_0208021923_0, QRD_0208021923_1);
    alldocuments.addAll(patient1PHMRdocuments);
    alldocuments.addAll(patient2PHMRdocuments);
    alldocuments.addAll(patient1QRDdocuments);
    alldocuments.addAll(patient2QRDdocuments);
    Map<String, List<String>> expectedMap = new HashMap<String, List<String>>();
    expectedMap.put(PATIENT1_CPR + PHMR_TYPE, patient1PHMRdocuments);
    expectedMap.put(PATIENT1_CPR + QRD_TYPE, patient1QRDdocuments);
    expectedMap.put(PATIENT2_CPR + PHMR_TYPE, patient2PHMRdocuments);
    expectedMap.put(PATIENT2_CPR + QRD_TYPE, patient2QRDdocuments);
    RepositoryConnectorDummy connector = new RepositoryConnectorDummy();
    MultipleSubmissionSetRepositoryDecorator decorator = new MultipleSubmissionSetRepositoryDecorator(connector);
    
    // Act
    decorator.provideAndRegisterCDADocuments(alldocuments, null, null);
    // Assert
    Assert.assertTrue(assertExpectedCdasMapWithActualCdas(expectedMap.values(), connector.getListOfCdas()));
  }

  @Test
  public void negativeTestOnDifferentPatientIds() throws Exception {
    // Assert
    List<String> documentsWithMixedPatientIds = Arrays.asList(PHMR_0101062112_0, PHMR_0208021923_0);
    Map<String, List<String>> expectedMap = new HashMap<String, List<String>>();
    expectedMap.put(PATIENT1_CPR + PHMR_TYPE, Arrays.asList(PHMR_0101062112_0));
    RepositoryConnectorDummy connector = new RepositoryConnectorDummy();
    MultipleSubmissionSetRepositoryDecorator decorator = new MultipleSubmissionSetRepositoryDecorator(connector);
    // Act
    decorator.provideAndRegisterCDADocuments(documentsWithMixedPatientIds, null, null);
    // Assert
    Assert.assertFalse(assertExpectedCdasMapWithActualCdas(expectedMap.values(), connector.getListOfCdas()));
  }
  
  @Test
  public void negativeTestOnDifferentDocumentTypes() throws Exception {
    // Arrange
    List<String> documentsWithMixedTypes = Arrays.asList(PHMR_0101062112_0, QRD_0101062112_0);
    Map<String, List<String>> expectedMap = new HashMap<String, List<String>>();
    expectedMap.put(PATIENT1_CPR + PHMR_TYPE, documentsWithMixedTypes);
    RepositoryConnectorDummy connector = new RepositoryConnectorDummy();
    MultipleSubmissionSetRepositoryDecorator decorator = new MultipleSubmissionSetRepositoryDecorator(connector);
    // Act
    decorator.provideAndRegisterCDADocuments(documentsWithMixedTypes, null, null);
    // Assert
    assertExpectedCdasMapWithActualCdas(expectedMap.values(), connector.getListOfCdas());
  }
  
  private boolean assertExpectedCdasMapWithActualCdas(
      Collection<List<String>> listOfExpectedCdas, Collection<List<String>> listOfActualCdas) {
    for (List<String> actualCdas : listOfActualCdas) {
      boolean actualCdasEqualsExpectedCdas = false;
      for (List<String> expectedCdas : listOfExpectedCdas) {
        if (actualCdas.size() == expectedCdas.size()) {
          actualCdasEqualsExpectedCdas |= compareContentOfLists(expectedCdas, actualCdas);
        }
      }
      if (!actualCdasEqualsExpectedCdas) {
        return false;
      }
    }
    return true;
  }
  
  private boolean compareContentOfLists(List<String> expectedCdas,
      List<String> actualCdas) {
    for (String actualCda : actualCdas) {
      boolean actualCdaFoundInExpected = false;
      for (String expectedCda : expectedCdas) {
        if (expectedCda.equals(actualCda)) {
          actualCdaFoundInExpected = true;
          break;
        }
      }
      if (!actualCdaFoundInExpected) {
        return false;
      }
    }
    return true;
  }
  
  /**
   * 
   * Collects the lists of cdas given as parameter to provideAndRegisterCDADocuments
   *
   */
  private class RepositoryConnectorDummy implements IRepositoryConnector {
    private List<List<String>> listOfCdas;
    
    public RepositoryConnectorDummy() {
      this.listOfCdas = new ArrayList<List<String>>(); 
    }
    
    public List<List<String>> getListOfCdas() {
      return listOfCdas;
    }

    @Override
    public RetrieveDocumentSetResponseType retrieveDocumentSet(String docId) {
      return null;
    }

    @Override
    public RegistryResponseType provideAndRegisterCDADocument(Document cda,
        CodedValue healthcareFacilityType, CodedValue practiceSettingsCode) {
      return null;
    }

    @Override
    public RegistryResponseType provideAndRegisterCDADocument(String cda,
        CodedValue healthcareFacilityType, CodedValue practiceSettingsCode) {
      return null;
    }
    
    @Override
    public RegistryResponseType provideAndRegisterCDADocuments(
        List<String> cdas, CodedValue healthcareFacilityType,
        CodedValue practiceSettingsCode) {
      listOfCdas.add(cdas);
      return createSuccessResponse();
    }
    
    private RegistryResponseType createSuccessResponse() {
      RegistryResponseType responseType = new RegistryResponseType();
      responseType.setStatus("urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success");
      return responseType;
    }
  }
}
