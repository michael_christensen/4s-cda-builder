package dk.s4.hl7.cda.upload;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.net4care.xdsconnector.IRepositoryConnector;
import org.net4care.xdsconnector.Utilities.CodedValue;
import org.net4care.xdsconnector.service.RegistryResponseType;
import org.net4care.xdsconnector.service.RetrieveDocumentSetResponseType;
import org.w3c.dom.Document;

public class RetryRepositoryDecoratorTest {
  
  @Test
  public void successTest() throws Exception {
    int nofExpectedRetries = 5;
    List<String> alldocuments = Arrays.asList("DummyDocument");
    
    RepositoryConnectorDummy connector = new RepositoryConnectorDummy(nofExpectedRetries);
    int[] sleepList = new int[] {1, 1, 1, 2};
    long totalSleep = calculateTotalSleep(nofExpectedRetries, sleepList);
    
    RetryRepositoryDecorator decorator = new RetryRepositoryDecorator(sleepList, connector);
    actAndAssert(nofExpectedRetries, alldocuments, connector, totalSleep, decorator);
  }

  private void actAndAssert(int nofExpectedRetries, List<String> alldocuments,
      RepositoryConnectorDummy connector, long totalSleep,
      RetryRepositoryDecorator decorator) {
    long start = System.currentTimeMillis();
    decorator.provideAndRegisterCDADocuments(alldocuments, null, null);
    long end = System.currentTimeMillis();
    
    assertRetryCountAndTotalSleep(nofExpectedRetries, connector, totalSleep, start, end);
  }

  private void assertRetryCountAndTotalSleep(int nofExpectedRetries,
      RepositoryConnectorDummy connector, long totalSleep, long start, long end) {
    Assert.assertEquals(totalSleep, (end - start) / 1000);
    Assert.assertEquals(connector.getCountNOFInvoked(), nofExpectedRetries);
  }

  private long calculateTotalSleep(int nofExpectedRetries, int[] sleepList) {
    return sum(sleepList) + (nofExpectedRetries > sleepList.length ? (nofExpectedRetries - sleepList.length) * sleepList[sleepList.length - 1] : 0);
  }
  
  private long sum(int[] list) {
    long sum = 0; 
    for (int i : list) {
        sum = sum + i;
    }
    return sum;
}
  
  private class RepositoryConnectorDummy implements IRepositoryConnector {
    private int castExceptionCount;
    private int countNofInvoked;
    
    public RepositoryConnectorDummy(int castExceptionCount) {
      this.castExceptionCount = castExceptionCount;
      countNofInvoked = 0;
    }
    
    public int getCountNOFInvoked() {
      return countNofInvoked;
    }

    @Override
    public RetrieveDocumentSetResponseType retrieveDocumentSet(String docId) {
      return null;
    }

    @Override
    public RegistryResponseType provideAndRegisterCDADocument(Document cda,
        CodedValue healthcareFacilityType, CodedValue practiceSettingsCode) {
      return null;
    }

    @Override
    public RegistryResponseType provideAndRegisterCDADocument(String cda,
        CodedValue healthcareFacilityType, CodedValue practiceSettingsCode) {
      return null;
    }

    @Override
    public RegistryResponseType provideAndRegisterCDADocuments(
        List<String> cdas, CodedValue healthcareFacilityType,
        CodedValue practiceSettingsCode) {
      if (countNofInvoked < castExceptionCount) {
        countNofInvoked++;
        throw new RuntimeException("Expected exception that forced retry");
      }
      return createSuccessResponse();
    }
    
    private RegistryResponseType createSuccessResponse() {
      RegistryResponseType responseType = new RegistryResponseType();
      responseType.setStatus("urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success");
      return responseType;
    }
  }
}
