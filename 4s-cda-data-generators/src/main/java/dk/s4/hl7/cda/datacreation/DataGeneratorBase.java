package dk.s4.hl7.cda.datacreation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.joda.time.DateTime;

/**
 * Base class that contains general methods for the DataGenerator classes.
 * 
 */
public abstract class DataGeneratorBase extends DataCreationBase {
  // Default is 1, if not the qrdOrPhmrArgument is set.
  int documentsPerPatient = 1;
  // dataHeader contains the header information of the specific QRD or PHMR data.
  private String  dataHeader = "";
  // This contains all the specific QRD or PHMR data. 
  private List<String> dataSet = new ArrayList<String>();
  //This contains the header data information for each person
  private HashMap<String, String> personDataMap = new HashMap<String,String>();
  // headerPerson contains the header information as string for the person
  protected String headerPersonAndData = "";
  // value for the size of persons that we are going to handle.   
  protected int personSize = 0;
  
  //Contains the property file configuration
  DateTime propertyTimestamp;
  
  //Contains the calulated phmr timestamp
  DateTime propertyCalculatedTimestamp;
  
  // If we are generation QRD documents this is set.
  protected boolean generateQrqDocuments;
  
  //If we are generation PHMR documents this is set.
  protected boolean generatePhmrDocuments;
  
  
  
  protected  void parseCsvFile(String filesPath, String outputFileName, boolean isQrd) throws IOException {
    if (isQrd) {
      documentsPerPatient = Integer.parseInt(qrdNumberDocumentsPerPatient.trim());    
    } else {
      documentsPerPatient = Integer.parseInt(phmrNumberDocumentsPerPatient.trim());
    }
    
    List<CSVRecord> generatedCdaHeaderDataRecords = getHeaderData();
     
    getSpecificData(filesPath);
    
    getHeaderData(generatedCdaHeaderDataRecords);    
         
    writeCsvFile(headerPersonAndData,outputFileName,documentsPerPatient);
  }


  private List<CSVRecord> getHeaderData() throws IOException {
    String generatedCdaHeaderDataPathCsv = generatedCdaHeaderDataPath + "generatedCdaHeaderDataFile.csv";
    List<CSVRecord> generatedCdaHeaderDataRecords = getCsvRecords(generatedCdaHeaderDataPathCsv);
    personSize = generatedCdaHeaderDataRecords.size() - 1;
    return generatedCdaHeaderDataRecords;
  }
  
  
  /**
   * Sets the personDataMap, first key value is the person id "patient cpr" and the  value is the header data, for each row
   * Creates headerPersonAndData, that is the all the header information from person and the specific data header "QRD" or "PHMR" 
   * @param generatedCdaHeaderDataRecords 
   * 
   * @throws UnsupportedEncodingException
   * @throws FileNotFoundException
   * @throws IOException
   *
   */
  private void getHeaderData(List<CSVRecord> generatedCdaHeaderDataRecords)  {
    
    
    int generatedCdaHeaderDataRecordNumber = 0;
    int generatedCdaHeaderDataRecordsize = 0;
    String personString = "";
    int patientCprIndex = 0; 
    
    // For hver headerData række
    for (CSVRecord generatedCdaHeaderDataRecord : generatedCdaHeaderDataRecords) {
      if(generatedCdaHeaderDataRecordNumber == 0) {
        generatedCdaHeaderDataRecordNumber++;
        generatedCdaHeaderDataRecordsize = generatedCdaHeaderDataRecord.size();
        for (int i = 0; i < generatedCdaHeaderDataRecord.size(); i++) {
          headerPersonAndData = headerPersonAndData + generatedCdaHeaderDataRecord.get(i) + ";" ;
          if (generatedCdaHeaderDataRecord.get(i).equalsIgnoreCase(CsvColumnNames.PATIENT_CPR.getColumnName())) {
            patientCprIndex = i;
          }
        }
        headerPersonAndData = headerPersonAndData + dataHeader + ";dokumentation_id;" + "objekt_id";
      } else {
        generatedCdaHeaderDataRecordNumber++;
        int generatedCdaDataRecordsize = generatedCdaHeaderDataRecord.size();
        int numberOfPersons = generatedCdaDataRecordsize/generatedCdaHeaderDataRecordsize;
        
        int personIndex = 0;
        String patientCpr = "";
        for (int i = 0; i < numberOfPersons; i++) {
          for (int j = 0; j < generatedCdaHeaderDataRecordsize; j++) {
            personString = personString + generatedCdaHeaderDataRecord.get(personIndex) + ";";
            if (j == patientCprIndex) {
                 patientCpr = generatedCdaHeaderDataRecord.get(patientCprIndex);
            }
            personIndex++;
          }
          personDataMap.put(patientCpr,personString); 
          personString = "";   
        }
      }
    }
  }
  
  /**
   * Get specific data from QRD or PHMR and sets the header in dataHeader & the data records in dataSet
   * 
   * @param filesPath
   * @throws UnsupportedEncodingException
   * @throws FileNotFoundException
   * @throws IOException
   */
  private void getSpecificData(String filesPath) throws UnsupportedEncodingException, FileNotFoundException, IOException {
    
    
    propertyCalculatedTimestamp = propertyTimestamp;
        
    File[] fileListAll = listFilesFromDirectory(filesPath);
    
    //Pick only one file, if you don´t want to iterate over only one file
    // remove this random lines and change fileListAll to fileList.
    int randomValueChooseFile = (int) (Math.random() * (fileListAll.length - 1)); // get random between 0 and list length - 1
    File[] fileList = {fileListAll[randomValueChooseFile]};
    
    // Generate specific PHMR or QRD data for each file.
    for (int fileIndex = 0; fileIndex < fileList.length; fileIndex++) {
      //if there are many documents for each person we want to create, create random data for this.
      //Reset the phmrPropertyCalculatedTimestamp for each file.
     for (int currentDocumentCount = 0; currentDocumentCount < documentsPerPatient; currentDocumentCount++) {
        String filesPathCsv = fileList[fileIndex].getAbsolutePath();
        List<CSVRecord> fileDataRecords = getCsvRecordsWithHeader(filesPathCsv);
        if (!fileDataRecords.isEmpty()) {
          dataHeader = copyHeaderFromEachCSV(fileDataRecords);
          processDataRecord(fileDataRecords, dataSet);
        }
      }
    }
  }

 /**
  * Get the Reader, the is special setup its we want to generate  many files and data, then we need the header information
  * 
  * @param reader
  * @return CSVRecord
  * @throws IOException
  */

  protected  List<CSVRecord> getCsvRecordsWithHeader(String fileName) throws IOException {
    if (generateQrqDocuments) {
      return getCsvRecords(fileName, "UTF-8", CsvColumnNames.QRD_DOK_TITEL.getColumnName(), CsvColumnNames.QRD_SKABELON.getColumnName() ,CsvColumnNames.QRD_SPOERGSMAAL_ID.getColumnName(),
          CsvColumnNames.QRD_TYPE.getColumnName(),CsvColumnNames.QRD_SVAR.getColumnName(),CsvColumnNames.QRD_TITEL.getColumnName() ,
          CsvColumnNames.QRD_VEJLEDENDE_TEKST.getColumnName() ,CsvColumnNames.QRD_SPOERGSMAAL_TEXT.getColumnName() ,CsvColumnNames.QRD_ANTAL.getColumnName());
    }
    if (generatePhmrDocuments) {
      return getCsvRecords(fileName, "UTF-8",CsvColumnNames.PHMR_TIDSPUNKT.getColumnName(),CsvColumnNames.PHMR_TYPE.getColumnName(),
          CsvColumnNames.PHMR_ENHED.getColumnName(),CsvColumnNames.PHMR_VAERDI.getColumnName(),
          CsvColumnNames.MEDICAL_DEVICE_CODE.getColumnName(),CsvColumnNames.MEDICAL_DEVICE_DISPLAYNAME.getColumnName(),
          CsvColumnNames.MANUFACTURER_MODEL_NAME.getColumnName(),CsvColumnNames.PHMR_MAALINGER_PATIENT.getColumnName());
    }
    return getCsvRecords(fileName, "UTF-8");
  }
   
  /**
   * This method creates the csv file when generating QRD data or PHMR data.
   * 
   * @param headerString contains the String for the header
   * @param HeaderMap contains patient id, for the file name, and the person record to be written.
   * @param dataSet contains the qrd or phmr specific data.
   * @param outputFileName is the file name, with path. 
   * @param documentsPerPatient 
   * @param sizeDocumentsPerPatient how many document for each patient to create
   * @throws IOException
   * 
   */
  
    
  private  void writeCsvFile(String headerString,String outputFileName, int sizeDocumentsPerPatient) throws IOException {
     
    int dataSetNumber = 0;
    
    for (Entry<String, String> entry : personDataMap.entrySet()) {
      
      for (int i = 0; i < sizeDocumentsPerPatient; i++) {
        String key = entry.getKey();
        String value = entry.getValue();
        
        if (sizeDocumentsPerPatient > 1) { // if size is bigger then 1, we generate x number of  documents, and we prefix with _i
          key = key + "_" +  i;
        }

        String outputFileNameWithKey = outputFileName + key + ".csv";
        new File(outputFileNameWithKey).getParentFile().mkdirs();

        Writer out = new BufferedWriter(new OutputStreamWriter(
            new FileOutputStream(outputFileNameWithKey), "UTF-8"));

        CSVFormat outFormat = CSVFormat.EXCEL.withHeader(headerString);
        CSVPrinter outPrinter = new CSVPrinter(new PrintWriter(out), outFormat);
        
        int sizeDatarecords = dataSet.size();
                     
        int sizeDatarecordsPerPatient = sizeDatarecords / personSize /  documentsPerPatient;
               
        for (int j=0; j < sizeDatarecordsPerPatient; j++) {
          out.write(value + dataSet.get(dataSetNumber) + "\n");
          dataSetNumber++;
        }
                      
        outPrinter.flush();
        outPrinter.close();
      }

    }

  }
  
  protected String splitData(CSVRecord dataRecord, String docId) {
    StringBuilder csvRecordStringBuilder = new StringBuilder();
    for (int i = 0; i < dataRecord.size(); i++) {
      csvRecordStringBuilder.append(dataRecord.get(i)).append(';');
    }
    
    String objId = UUID.randomUUID().toString();
    csvRecordStringBuilder.append(docId + ";" + objId  + ";");
    
    if (csvRecordStringBuilder.length() > 0) { 
      csvRecordStringBuilder.setLength(csvRecordStringBuilder.length() - 1); 
    }
    
    
    return csvRecordStringBuilder.toString();
  }
  
  protected abstract void processDataRecord(List<CSVRecord> dataRecords, List<String> dataSet);
}
