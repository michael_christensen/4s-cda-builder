package dk.s4.hl7.cda.datacreation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.apache.commons.csv.CSVRecord;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

/**
 * Converts QRD specific data and header information csv files, to new csv file,
 * with all the information.
 * 
 */
public class QrdDataGenerator extends DataGeneratorBase {
  public static void main(String[] args) throws Exception {
    QrdDataGenerator qrdDataGenerator = new QrdDataGenerator();
    qrdDataGenerator.generateQrqDocuments = true;
    qrdDataGenerator.readProperty(args);

    // first timestamp from properties file
    qrdDataGenerator.propertyTimestamp = DateTime.parse(qrdDataGenerator.phmrTidspunkt,
        DateTimeFormat.forPattern("yyyy-MM-dd"));

    qrdDataGenerator.parseCsvFile();
  }

  @Override
  protected void parseCsvFile() throws IOException {
    String outputFileName = generatedQrdDataFilesPath + "QrdDataFileFor";
    final boolean isQrd = true;
    parseCsvFile(qrdFilesPath, outputFileName, isQrd);
  }

  /**
   * The QRD specific data is computed when this method is called.
   * 
   * @param filesDataRecord
   *          , contains the specific QRD data.
   * @param docId
   * @return String that contains the new specific QRD data as csv string.
   * 
   */
  protected String manipulateData(CSVRecord filesDataRecord, String docId) {
    String qrdDocTitel = filesDataRecord.get(CsvColumnNames.QRD_DOK_TITEL.getColumnName());
    String qrdSkabelon = filesDataRecord.get(CsvColumnNames.QRD_SKABELON.getColumnName());
    String qrdSpoergsmaalId = filesDataRecord.get(CsvColumnNames.QRD_SPOERGSMAAL_ID.getColumnName());
    String qrdType = filesDataRecord.get(CsvColumnNames.QRD_TYPE.getColumnName());
    String qrdSvar = filesDataRecord.get(CsvColumnNames.QRD_SVAR.getColumnName());
    String qrdTitel = filesDataRecord.get(CsvColumnNames.QRD_TITEL.getColumnName());
    String qrdVejledendeText = filesDataRecord.get(CsvColumnNames.QRD_VEJLEDENDE_TEKST.getColumnName());
    String qrdSpoegsmaalText = filesDataRecord.get(CsvColumnNames.QRD_SPOERGSMAAL_TEXT.getColumnName());
    String qrdAntal = filesDataRecord.get(CsvColumnNames.QRD_ANTAL.getColumnName());

    String[] split = qrdSvar.split("#");
    List<String> listSplit = new ArrayList<String>(Arrays.asList(split));

    // the multiple, discrete and text split up in # , and if the qrdAntal
    // is less then the text numbers, then select random on the text
    if (qrdType.equalsIgnoreCase("multiple") || qrdType.equalsIgnoreCase("discrete")
        || qrdType.equalsIgnoreCase("text")) {
      int sizeQrdAntal = Integer.parseInt(qrdAntal);
      for (; sizeQrdAntal < listSplit.size();) {
        int randomValue = (int) ((Math.random() * (listSplit.size() - 0)) + 1);
        listSplit.remove(--randomValue);
      }
      qrdSvar = "";
      for (String svar : listSplit) {
        qrdSvar = qrdSvar + svar + "#";
      }
      qrdSvar = removeLastCharacter(qrdSvar);
    }

    String objId = UUID.randomUUID().toString();

    String qrdTid = propertyCalculatedTimestamp.toString();

    return qrdDocTitel + ";" + qrdSkabelon + ";" + qrdSpoergsmaalId + ";" + qrdType + ";" + qrdSvar + ";" + qrdTitel
        + ";" + qrdVejledendeText + ";" + qrdSpoegsmaalText + ";" + qrdAntal + ";" + qrdTid + ";" + docId + ";" + objId;

  }

  private String removeLastCharacter(String qrdSvar) {
    // removes the last #
    int qrdSvarLength = qrdSvar.length();
    return qrdSvar.substring(0, --qrdSvarLength);

  }

  @Override
  protected void processDataRecord(List<CSVRecord> dataRecords, List<String> dataSet) {
    int recordsPerPatiens = (dataRecords.size() - 1) * documentsPerPatient;
    for (int i = 0; i < personSize; i++) {
      propertyCalculatedTimestamp = propertyCalculatedTimestamp.plusDays(Integer.parseInt(qrdAntalDageMellemMaaling
          .trim()));
      String docId = UUID.randomUUID().toString();
      for (int recordIndex = 1; recordIndex < dataRecords.size(); recordIndex++) {
        int remeinder = dataSet.size() % recordsPerPatiens;
        if (remeinder == 0) {
          propertyCalculatedTimestamp = propertyTimestamp;
        }
        dataSet.add(manipulateData(dataRecords.get(recordIndex), docId));
      }
    }
  }
}
