package dk.s4.hl7.cda.datacreation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Date;
import java.util.List;

import org.apache.commons.csv.CSVRecord;
import org.joda.time.DateTime;

import dk.s4.hl7.cda.codes.BasicType;
import dk.s4.hl7.cda.codes.IntervalType;
import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.convert.QRDXmlConverter;
import dk.s4.hl7.cda.model.AddressData;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.CodedValue.CodedValueBuilder;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.Reference;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.qrd.QRDAnalogSliderResponse;
import dk.s4.hl7.cda.model.qrd.QRDDiscreteSliderResponse;
import dk.s4.hl7.cda.model.qrd.QRDDocument;
import dk.s4.hl7.cda.model.qrd.QRDMultipleChoiceResponse;
import dk.s4.hl7.cda.model.qrd.QRDNumericResponse;
import dk.s4.hl7.cda.model.qrd.QRDResponse;
import dk.s4.hl7.cda.model.qrd.QRDTextResponse;

/**
 * Converts QRD object model to xml.
 * 
 */
public class QrdXmlCdaGenerator extends DataCreationBase {
  private QRDXmlConverter qrdXmlConverter = new QRDXmlConverter();

  public static void main(String[] args) throws Exception {
    QrdXmlCdaGenerator qrdCdaGenerator = new QrdXmlCdaGenerator();
    qrdCdaGenerator.readProperty(args);
    qrdCdaGenerator.parseCsvFile();
  }

  @Override
  protected void parseCsvFile() throws IOException {
    File[] qrdFiles = listFilesFromDirectory(generatedQrdDataFilesPath);
    for (File qrdFile : qrdFiles) {
      List<CSVRecord> generatedQrdFilesDateRecords = getCsvRecords(qrdFile.getAbsolutePath());
      QRDDocument qrdDocument = null;
      // Skip header row
      if (!generatedQrdFilesDateRecords.isEmpty()) { // Only do once
        qrdDocument = buildHeader(generatedQrdFilesDateRecords.get(1));
      }

      for (int recordIndex = 1; recordIndex < generatedQrdFilesDateRecords.size(); recordIndex++) {
        qrdDocument = writeQrdData(generatedQrdFilesDateRecords.get(recordIndex), qrdDocument);
      }
      String createQRDFromCDA = createQRDFromCDA(qrdDocument);

      writeXmlFile(createQRDFromCDA, qrdFile.getName());
    }
  }

  private QRDDocument buildHeader(CSVRecord qrdFilesDataRecord) {
    // Header
    String qrdDokTitel = qrdFilesDataRecord.get(CsvColumnNames.QRD_DOK_TITEL.getColumnName());
    // Organisation
    String organisationAfdelingStednavn = qrdFilesDataRecord.get(CsvColumnNames.ORGANISATION_AFDELING_STEDNAVN
        .getColumnName());
    String organisationAfdelingVejnavn = qrdFilesDataRecord.get(CsvColumnNames.ORGANISATION_AFDELING_VEJNAVN
        .getColumnName());
    String organisationAfdelingPostnummer = qrdFilesDataRecord.get(CsvColumnNames.ORGANISATION_AFDELING_POSTNUMMER
        .getColumnName());
    String organisationAfdelingBy = qrdFilesDataRecord.get(CsvColumnNames.ORGANISATION_AFDELING_BY.getColumnName());
    String organisationAnsvarligFornavn = qrdFilesDataRecord.get(CsvColumnNames.ORGANISATION_ANSVARLIG_FORNAVN
        .getColumnName());
    String organisationAnsvarligEfternavn = qrdFilesDataRecord.get(CsvColumnNames.ORGANISATION_ANSVARLIG_EFTERNAVN
        .getColumnName());
    String organisationNavn = qrdFilesDataRecord.get(CsvColumnNames.ORGANISATION_NAVN.getColumnName());

    // Sor
    String organisationAfdelingSor = qrdFilesDataRecord.get(CsvColumnNames.ORGANISATION_AFDELING_SOR.getColumnName());
    String organisationSor = qrdFilesDataRecord.get(CsvColumnNames.ORGANISATION_SOR.getColumnName());

    // Cusodian
    String custodianSor = qrdFilesDataRecord.get(CsvColumnNames.CUSTODIAN_SOR.getColumnName());
    String custodianStednavn = qrdFilesDataRecord.get(CsvColumnNames.CUSTODIAN_STEDNAVN.getColumnName());
    String custodianAfdelingNavn = qrdFilesDataRecord.get(CsvColumnNames.CUSTODIAN_AFDELING_NAVN.getColumnName());
    String custodianVejnavn = qrdFilesDataRecord.get(CsvColumnNames.CUSTODIAN_VEJ_NAVN.getColumnName());
    String custodianPostnummer = qrdFilesDataRecord.get(CsvColumnNames.CUSTODIAN_POSTNUMMER.getColumnName());
    String custodianBy = qrdFilesDataRecord.get(CsvColumnNames.CUSTODIAN_BY.getColumnName());

    // Period
    String qrdTime = qrdFilesDataRecord.get(CsvColumnNames.QRD_TID.getColumnName());

    Date documentCreationTime;
    if (qrdTime != null && qrdTime.trim().length() > 0) {
      documentCreationTime = DateTime.parse(qrdTime).toDate();
    } else {
      documentCreationTime = new Date();
    }

    // Patient
    Patient person = definePatientPersonIdentity(qrdFilesDataRecord);

    // Use the document_id from the csv file.
    String dokumentationId = qrdFilesDataRecord.get("dokumentation_id");

    QRDDocument qrdDocument = new QRDDocument(MedCom.createId(dokumentationId));
    qrdDocument.setTitle(qrdDokTitel);
    qrdDocument.setLanguageCode("da-DK");
    qrdDocument.setEffectiveTime(documentCreationTime);
    qrdDocument.setPatient(person);

    // Populate author
    qrdDocument.setAuthor(new ParticipantBuilder()
        .setCPR(person.getSSN())
        .setAddress(person.getAddress())
        .setPersonIdentity(person)
        .setTime(documentCreationTime)
        .build());

    AddressData custodianAdress = new AddressData.AddressBuilder(custodianPostnummer, custodianBy)
        .addAddressLine(custodianAfdelingNavn)
        .addAddressLine(custodianVejnavn)
        .setCountry("Danmark")
        .setUse(AddressData.Use.WorkPlace)
        .build();

    OrganizationIdentity custodianOrganization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR(custodianSor)
        .setName(custodianStednavn)
        .setAddress(custodianAdress)
        .addTelecom(Use.WorkPlace, "tel", "65223344")
        .build();

    qrdDocument.setCustodian(custodianOrganization);

    qrdDocument.addSection(new Section<QRDResponse>("Vejledning",
        "I forbindelse med dine netop udførte målinger skal vi bede dig besvare følgende spørgsmål.", "da-DK"));

    return qrdDocument;
  }

  private QRDDocument writeQrdData(CSVRecord qrdFilesDataRecord, QRDDocument qrdDocument) throws IOException {
    String referencer = qrdFilesDataRecord.get(CsvColumnNames.REFERENCER.getColumnName());
    String qrdSkabelon = qrdFilesDataRecord.get(CsvColumnNames.QRD_SKABELON.getColumnName());
    String qrdSpoergsmaelId = qrdFilesDataRecord.get(CsvColumnNames.QRD_SPOERGSMAAL_ID.getColumnName());
    String qrdObservationObjectId = qrdFilesDataRecord.get(CsvColumnNames.OBJEKT_ID.getColumnName());
    String qrdType = qrdFilesDataRecord.get(CsvColumnNames.QRD_TYPE.getColumnName());
    String qrdSvar = qrdFilesDataRecord.get(CsvColumnNames.QRD_SVAR.getColumnName());
    String qrdTitel = qrdFilesDataRecord.get(CsvColumnNames.QRD_TITEL.getColumnName());
    String qrdVejledendeText = qrdFilesDataRecord.get(CsvColumnNames.QRD_VEJLEDENDE_TEKST.getColumnName());
    String qrdSpoegsmaalText = qrdFilesDataRecord.get(CsvColumnNames.QRD_SPOERGSMAAL_TEXT.getColumnName());

    Section<QRDResponse> qRDSection = new Section<QRDResponse>(qrdTitel, qrdVejledendeText);
    CodedValue codeValue = new CodedValue(qrdSpoergsmaelId, MedCom.DEVICE_OID, qrdSpoegsmaalText, "MedCom prompt table");

    ID observationId = new ID.IDBuilder()
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .setExtension(qrdObservationObjectId)
        .setRoot(MedCom.ROOT_OID)
        .build();

    // Reference to QFDD
    Reference reference = null;
    if (referencer.equalsIgnoreCase("JA")) {
      CodedValue qfddCodedValue = new CodedValueBuilder()
          .setCode(Loinc.QFD_CODE)
          .setCodeSystem(Loinc.OID)
          .setDisplayName(Loinc.QFD_DISPLAYNAME)
          .build();
      ID referenceId = new ID.IDBuilder()
          .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
          .setExtension(qrdSkabelon)
          .setRoot(MedCom.DK_REFERENCE_EXTERNAL_URL)
          .build();
      reference = new Reference.ReferenceBuilder(Reference.DocumentIdReferencesUse.HTTP_REFERENCE.getReferencesUse(),
          referenceId, qfddCodedValue).build();
    }

    if (qrdType.equalsIgnoreCase("discrete")) {
      QRDDiscreteSliderResponse qRDResponse = new QRDDiscreteSliderResponse.QRDDiscreteSliderResponseBuilder()
          .setMinimum("1")
          .setCodeValue(codeValue)
          .setId(observationId)
          .setQuestion(qrdSpoegsmaalText)
          .build();
      qRDResponse.setAnswer("A1", MedCom.DEVICE_OID, qrdSvar, "MedCom prompt table");

      updateDocument(qrdDocument, qRDSection, reference, qRDResponse);

    }
    if (qrdType.equalsIgnoreCase("numeric")) {
      String[] qrdSvarNumeric = qrdSvar.split("#");
      if (qrdSvarNumeric.length != 2) {
        throw new IOException("The type numeric must have 2 parameters, min#max, example: 1#100");
      }
      int low = Integer.parseInt(qrdSvarNumeric[0]);
      int high = Integer.parseInt(qrdSvarNumeric[1]);
      int randomValue = (int) ((Math.random() * (high - low)) + low);

      QRDResponse qRDResponse = new QRDNumericResponse.QRDNumericResponseBuilder()
          .setInterval(qrdSvarNumeric[0], qrdSvarNumeric[1], IntervalType.IVL_INT)
          .setValue(String.valueOf(randomValue), BasicType.INT)
          .setCodeValue(codeValue)
          .setId(observationId)
          .setQuestion(qrdSpoegsmaalText)
          .build();

      updateDocument(qrdDocument, qRDSection, reference, qRDResponse);

    }
    if (qrdType.equalsIgnoreCase("text")) {
      QRDResponse qRDResponse = new QRDTextResponse.QRDTextResponseBuilder()
          .setText(qrdSvar)
          .setCodeValue(codeValue)
          .setId(observationId)
          .setQuestion(qrdSpoegsmaalText)
          .build();

      updateDocument(qrdDocument, qRDSection, reference, qRDResponse);
    }
    if (qrdType.equalsIgnoreCase("analog")) {
      String[] qrdSvarAnalog = qrdSvar.split("#");
      if (qrdSvarAnalog.length != 3) {
        throw new IOException("The type analog must have 3 parameters, min#max#increment, example: 0#100#1");
      }

      int low = Integer.parseInt(qrdSvarAnalog[0]);
      int high = Integer.parseInt(qrdSvarAnalog[1]);
      int randomValue = (int) ((Math.random() * (high - low)) + low);

      QRDResponse qRDResponse = new QRDAnalogSliderResponse.QRDAnalogSliderResponseBuilder()
          .setValue(String.valueOf(randomValue), "INT")
          .setInterval(qrdSvarAnalog[1], qrdSvarAnalog[0], qrdSvarAnalog[2])
          .setCodeValue(codeValue)
          .setId(observationId)
          .setQuestion(qrdSpoegsmaalText)
          .build();

      updateDocument(qrdDocument, qRDSection, reference, qRDResponse);
    }
    if (qrdType.equalsIgnoreCase("multiple")) {
      String[] qrdSvarMultiple = qrdSvar.split("#");
      QRDMultipleChoiceResponse qRDResponse = new QRDMultipleChoiceResponse.QRDMultipleChoiceResponseBuilder()
          .setMinimum("1")
          .setMaximum("1")
          .setCodeValue(codeValue)
          .setId(observationId)
          .setQuestion(qrdSpoegsmaalText)
          .build();
      int low = 0;
      int high = qrdSvarMultiple.length;
      int randomValue = (int) ((Math.random() * (high - low)) + low);
      qRDResponse.addAnswer("A" + randomValue, "Some-ChoiceDomain-OID", qrdSvarMultiple[randomValue],
          "Some-CodeSystem-Name");
      updateDocument(qrdDocument, qRDSection, reference, qRDResponse);
    }

    return qrdDocument;

  }

  private void updateDocument(QRDDocument qrdDocument, Section<QRDResponse> qRDSection, Reference reference,
      QRDResponse qRDResponse) {
    qRDSection.addQuestionnaireEntity(qRDResponse);
    qrdDocument.addSection(qRDSection);
    if (reference != null) {
      qRDResponse.addReference(reference);
    }
  }

  public List<CSVRecord> getCsvRecords(String fileName) throws IOException {

    return getCsvRecords(fileName, "UTF-8", CsvColumnNames.ORGANISATION_AFDELING_SOR.getColumnName(),
        CsvColumnNames.ORGANISATION_AFDELING_STEDNAVN.getColumnName(),
        CsvColumnNames.ORGANISATION_AFDELING_VEJNAVN.getColumnName(),
        CsvColumnNames.ORGANISATION_AFDELING_POSTNUMMER.getColumnName(),
        CsvColumnNames.ORGANISATION_AFDELING_BY.getColumnName(),
        CsvColumnNames.ORGANISATION_ANSVARLIG_FORNAVN.getColumnName(),
        CsvColumnNames.ORGANISATION_ANSVARLIG_EFTERNAVN.getColumnName(),
        CsvColumnNames.ORGANISATION_SOR.getColumnName(), CsvColumnNames.ORGANISATION_NAVN.getColumnName(),
        CsvColumnNames.LEGAL_AUTHENTICATOR_AFDELING_SOR.getColumnName(),
        CsvColumnNames.LEGAL_AUTHENTICATOR_AFDELING_STEDNAVN.getColumnName(),
        CsvColumnNames.LEGAL_AUTHENTICATOR_AFDELING_VEJNAVN.getColumnName(),
        CsvColumnNames.LEGAL_AUTHENTICATOR_AFDELING_POSTNUMMER.getColumnName(),
        CsvColumnNames.LEGAL_AUTHENTICATOR_AFDELING_BY.getColumnName(),
        CsvColumnNames.LEGAL_AUTHENTICATOR_ANSVARLIG_FORNAVN.getColumnName(),
        CsvColumnNames.LEGAL_AUTHENTICATOR_ANSVARLIG_EFTERNAVN.getColumnName(),
        CsvColumnNames.LEGAL_AUTHENTICATOR_SOR.getColumnName(),
        CsvColumnNames.LEGAL_AUTHENTICATOR_NAVN.getColumnName(), CsvColumnNames.CUSTODIAN_SOR.getColumnName(),
        CsvColumnNames.CUSTODIAN_STEDNAVN.getColumnName(), CsvColumnNames.CUSTODIAN_AFDELING_NAVN.getColumnName(),
        CsvColumnNames.CUSTODIAN_VEJ_NAVN.getColumnName(), CsvColumnNames.CUSTODIAN_POSTNUMMER.getColumnName(),
        CsvColumnNames.CUSTODIAN_BY.getColumnName(), CsvColumnNames.PATIENT_CPR.getColumnName(),
        CsvColumnNames.PATIENT_FORNANVN.getColumnName(), CsvColumnNames.PATIENT_EFTERNAVN.getColumnName(),
        CsvColumnNames.PATIENT_VEJNAVN.getColumnName(), CsvColumnNames.PATIENT_POSTNUMMER.getColumnName(),
        CsvColumnNames.PATIENT_BY.getColumnName(), CsvColumnNames.PATIENT_TLF.getColumnName(),
        CsvColumnNames.PATIENT_MAIL.getColumnName(), CsvColumnNames.REFERENCER.getColumnName(),
        CsvColumnNames.QRD_DOK_TITEL.getColumnName(), CsvColumnNames.QRD_SKABELON.getColumnName(),
        CsvColumnNames.QRD_SPOERGSMAAL_ID.getColumnName(), CsvColumnNames.QRD_TYPE.getColumnName(),
        CsvColumnNames.QRD_SVAR.getColumnName(), CsvColumnNames.QRD_TITEL.getColumnName(),
        CsvColumnNames.QRD_VEJLEDENDE_TEKST.getColumnName(), CsvColumnNames.QRD_SPOERGSMAAL_TEXT.getColumnName(),
        CsvColumnNames.QRD_ANTAL.getColumnName(), CsvColumnNames.QRD_TID.getColumnName(),
        CsvColumnNames.DOKUMENTATION_ID.getColumnName(), CsvColumnNames.OBJEKT_ID.getColumnName());
  }

  private void writeXmlFile(String createQRDFromCDA, String fileName) throws IOException {
    String xmlFileName = fileName.replace("csv", "xml");
    String outputFileName = generatedQrdXmlFilesPath + xmlFileName;
    new File(outputFileName).getParentFile().mkdirs();
    Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFileName), "UTF-8"));
    out.write(createQRDFromCDA);
    out.flush();
    out.close();
  }

  public String createQRDFromCDA(QRDDocument cda) {
    return qrdXmlConverter.convert(cda);
  }
}
