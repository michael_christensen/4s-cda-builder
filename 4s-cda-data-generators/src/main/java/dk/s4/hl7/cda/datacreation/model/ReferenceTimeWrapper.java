package dk.s4.hl7.cda.datacreation.model;

import org.joda.time.DateTime;

import dk.s4.hl7.cda.model.Reference;

public class ReferenceTimeWrapper {
  private DateTime time;
  private Reference reference;
  
  public ReferenceTimeWrapper(DateTime time, Reference reference) {
    super();
    this.time = time;
    this.reference = reference;
  }

  public DateTime getTime() {
    return time;
  }

  public Reference getReference() {
    return reference;
  }
 
}
