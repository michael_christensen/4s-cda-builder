package dk.s4.hl7.cda.upload;

import java.io.StringWriter;
import java.util.List;
import java.util.concurrent.Callable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.net4care.xdsconnector.IRepositoryConnector;
import org.net4care.xdsconnector.Utilities.CodedValue;
import org.net4care.xdsconnector.service.ObjectFactory;
import org.net4care.xdsconnector.service.RegistryResponseType;
import org.net4care.xdsconnector.service.RetrieveDocumentSetResponseType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

public class RetryRepositoryDecorator implements IRepositoryConnector {
  public static final String REQUEST_SUCCESS_STATUS = "urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success";
  private static final Logger logger = LoggerFactory.getLogger(RetryRepositoryDecorator.class);
  private IRepositoryConnector nextRepositoryConnector;
  private int[] timeoutsInSeconds;
  
  public RetryRepositoryDecorator(int[] timeoutsInSeconds, IRepositoryConnector nextRepositoryConnector) {
    this.timeoutsInSeconds = timeoutsInSeconds;
    this.nextRepositoryConnector = nextRepositoryConnector;
  }

  @Override
  public RetrieveDocumentSetResponseType retrieveDocumentSet(String docId) {
    return retryStrategy(new RetrieveDocumentSetCallable(docId, nextRepositoryConnector));
  }

  @Override
  public RegistryResponseType provideAndRegisterCDADocument(Document cda,
      CodedValue healthcareFacilityType, CodedValue practiceSettingsCode) {
    return retryStrategy(new ProvideAndRegisterCDADocumentDOMCallable(cda, healthcareFacilityType, practiceSettingsCode, nextRepositoryConnector));
  }

  @Override
  public RegistryResponseType provideAndRegisterCDADocument(String cda,
      CodedValue healthcareFacilityType, CodedValue practiceSettingsCode) {
    return retryStrategy(new ProvideAndRegisterCDADocumentStringCallable(cda, healthcareFacilityType, practiceSettingsCode, nextRepositoryConnector));
  }

  @Override
  public RegistryResponseType provideAndRegisterCDADocuments(List<String> cdas,
      CodedValue healthcareFacilityType, CodedValue practiceSettingsCode) {
    return retryStrategy(new ProvideAndRegisterCDADocumentsCallable(cdas, healthcareFacilityType, practiceSettingsCode, nextRepositoryConnector));
  }
  
  private <E> E retryStrategy(Callable<E> callable) throws RuntimeException {
    int currentTimeoutIndex = 0;
    while(true) {
      try {
        return callable.call();
        } catch (Exception ex) {
          logger.error("Error on upload: " + ex.getMessage(), ex);
          int sleepSeconds = timeoutsInSeconds[currentTimeoutIndex];
          if (currentTimeoutIndex + 1 < timeoutsInSeconds.length) {
            currentTimeoutIndex++;
          }
          logger.info("Retry after " + sleepSeconds + " second(s)");            
          sleep(sleepSeconds);
        }
    }
  }

  private void sleep(int time) throws RuntimeException {
    try {
      Thread.sleep(time * 1000L);
    } catch (InterruptedException e) {
      throw new CDAUploaderException(e);
    }
  }
  
  private static void logResponse(RegistryResponseType response)
      throws JAXBException {
    JAXBContext context = JAXBContext.newInstance(RegistryResponseType.class);
    StringWriter writer = new StringWriter();
    context.createMarshaller().marshal(new ObjectFactory().createRegistryResponse(response), writer);
    logger.debug("ErrorResponse: " + writer.toString());
  }
  
  private class RetrieveDocumentSetCallable implements Callable<RetrieveDocumentSetResponseType> {
    private String docId;
    private IRepositoryConnector nextRepositoryConnector;
    
    public RetrieveDocumentSetCallable(String docId, IRepositoryConnector nextRepositoryConnector) {
      this.nextRepositoryConnector = nextRepositoryConnector;
      this.docId = docId;
    }

    @Override
    public RetrieveDocumentSetResponseType call() throws Exception {
      RetrieveDocumentSetResponseType response = nextRepositoryConnector.retrieveDocumentSet(docId);
      if (!REQUEST_SUCCESS_STATUS.equals(response.getRegistryResponse().getStatus())) {
        logResponse(response.getRegistryResponse());
        throw new CDAUploaderException("Repository response is an error");
      }
      return response;
    }
  }
  
  private class ProvideAndRegisterCDADocumentDOMCallable implements Callable<RegistryResponseType> {
    private Document cdaDocument;
    private CodedValue healthcareFacilityType;
    private CodedValue practiceSettingsCode;
    private IRepositoryConnector nextRepositoryConnector;
    
    public ProvideAndRegisterCDADocumentDOMCallable(Document cdaDocument,
        CodedValue healthcareFacilityType, CodedValue practiceSettingsCode, IRepositoryConnector nextRepositoryConnector) {
      this.nextRepositoryConnector = nextRepositoryConnector;
      this.cdaDocument = cdaDocument;
      this.healthcareFacilityType = healthcareFacilityType;
      this.practiceSettingsCode = practiceSettingsCode;
    }

    @Override
    public RegistryResponseType call() throws Exception {
      RegistryResponseType response = nextRepositoryConnector.provideAndRegisterCDADocument(cdaDocument, healthcareFacilityType, practiceSettingsCode);
      if (!REQUEST_SUCCESS_STATUS.equals(response.getStatus())) {
        logResponse(response);
        throw new CDAUploaderException("Repository response is an error");
      }
      return response;
    }


  }
  
  private class ProvideAndRegisterCDADocumentStringCallable implements Callable<RegistryResponseType> {
    private String cdaDocument;
    private CodedValue healthcareFacilityType;
    private CodedValue practiceSettingsCode;
    private IRepositoryConnector nextRepositoryConnector;
    
    public ProvideAndRegisterCDADocumentStringCallable(String cdaDocument,
        CodedValue healthcareFacilityType, CodedValue practiceSettingsCode, IRepositoryConnector nextRepositoryConnector) {
      this.nextRepositoryConnector = nextRepositoryConnector;
      this.cdaDocument = cdaDocument;
      this.healthcareFacilityType = healthcareFacilityType;
      this.practiceSettingsCode = practiceSettingsCode;
    }

    @Override
    public RegistryResponseType call() throws Exception {
      RegistryResponseType response = nextRepositoryConnector.provideAndRegisterCDADocument(cdaDocument, healthcareFacilityType, practiceSettingsCode);
      if (!REQUEST_SUCCESS_STATUS.equals(response.getStatus())) {
        logResponse(response);
        throw new CDAUploaderException("Repository response is an error");
      }
      return response;
    }
  }
  
  private class ProvideAndRegisterCDADocumentsCallable implements Callable<RegistryResponseType> {
    private List<String> cdaDocuments;
    private CodedValue healthcareFacilityType;
    private CodedValue practiceSettingsCode;
    private IRepositoryConnector nextRepositoryConnector;
    
    public ProvideAndRegisterCDADocumentsCallable(List<String> cdaDocuments,
        CodedValue healthcareFacilityType, CodedValue practiceSettingsCode, IRepositoryConnector nextRepositoryConnector) {
      this.nextRepositoryConnector = nextRepositoryConnector;
      this.cdaDocuments = cdaDocuments;
      this.healthcareFacilityType = healthcareFacilityType;
      this.practiceSettingsCode = practiceSettingsCode;
    }

    @Override
    public RegistryResponseType call() throws Exception {
      RegistryResponseType response =  nextRepositoryConnector.provideAndRegisterCDADocuments(cdaDocuments, healthcareFacilityType, practiceSettingsCode);
      if (!REQUEST_SUCCESS_STATUS.equals(response.getStatus())) {
        logResponse(response);
        throw new CDAUploaderException("Repository response is an error");
      }
      return response;
    }
  }
}
