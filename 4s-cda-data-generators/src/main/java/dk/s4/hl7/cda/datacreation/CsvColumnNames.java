package dk.s4.hl7.cda.datacreation;

public enum CsvColumnNames {
  ORGANISATION_AFDELING_SOR("organisation_afdeling_sor"),
  ORGANISATION_AFDELING_STEDNAVN("organisation_afdeling_stednavn"),
  ORGANISATION_AFDELING_VEJNAVN ("organisation_afdeling_vejnavn"),
  ORGANISATION_AFDELING_POSTNUMMER ("organisation_afdeling_postnummer"),
  ORGANISATION_AFDELING_BY("organisation_afdeling_by"),
  ORGANISATION_ANSVARLIG_FORNAVN("organisation_ansvarlig_fornavn"),
  ORGANISATION_ANSVARLIG_EFTERNAVN ("organisation_ansvarlig_efternavn"),
  ORGANISATION_SOR("organisation_sor"),
  ORGANISATION_NAVN("organisation_navn"),
  LEGAL_AUTHENTICATOR_AFDELING_SOR("legal_organisation_afdeling_sor"),
  LEGAL_AUTHENTICATOR_AFDELING_STEDNAVN("legal_organisation_afdeling_stednavn"),
  LEGAL_AUTHENTICATOR_AFDELING_VEJNAVN ("legal_organisation_afdeling_vejnavn"),
  LEGAL_AUTHENTICATOR_AFDELING_POSTNUMMER ("legal_organisation_afdeling_postnummer"),
  LEGAL_AUTHENTICATOR_AFDELING_BY("legal_organisation_afdeling_by"),
  LEGAL_AUTHENTICATOR_ANSVARLIG_FORNAVN("legal_organisation_ansvarlig_fornavn"),
  LEGAL_AUTHENTICATOR_ANSVARLIG_EFTERNAVN ("legal_organisation_ansvarlig_efternavn"),
  LEGAL_AUTHENTICATOR_SOR("legal_organisation_sor"),
  LEGAL_AUTHENTICATOR_NAVN("legal_organisation_navn"),
  CUSTODIAN_SOR("custodian_sor"),
  CUSTODIAN_STEDNAVN("custodian_stednavn"),
  CUSTODIAN_AFDELING_NAVN("custodian_afdeling_navn"),
  CUSTODIAN_VEJ_NAVN("custodian_vejnavn"),
  CUSTODIAN_POSTNUMMER("custodian_postnummer"),
  CUSTODIAN_BY("custodian_by"),
  PATIENT_CPR("patient_cpr"),
  PATIENT_FORNANVN("patient_fornavn"),
  PATIENT_EFTERNAVN("patient_efternavn"),
  PATIENT_VEJNAVN("patient_vejnavn"),
  PATIENT_POSTNUMMER("patient_postnummer"),
  PATIENT_BY("patient_by"),
  PATIENT_TLF("patient_tlf"),
  PATIENT_MAIL("patient_mail"),
  REFERENCER("Referencer"),
  DOKUMENTATION_ID("dokumentation_id"),
  OBJEKT_ID("objekt_id"),
//QRD specific data
  QRD_DOK_TITEL("qrd_dok_titel"),
  QRD_SKABELON("qrd_skabelon"),
  QRD_SPOERGSMAAL_ID("qrd_spørgsmål_id"),
  QRD_TYPE("qrd_type"),
  QRD_SVAR("qrd_svar"),
  QRD_TITEL("qrd_titel"),
  QRD_VEJLEDENDE_TEKST("qrd_vejledende_tekst"),
  QRD_SPOERGSMAAL_TEXT("qrd_spørgsmål_text"),
  QRD_ANTAL("qrd_antal"),
  QRD_TID ("qrd_tid"),
//PHMR specific data
  PHMR_TIDSPUNKT("phmr_tidspunkt"),  
  PHMR_TYPE("phmr_type"),  
  PHMR_ENHED("phmr_enhed"),  
  PHMR_VAERDI("phmr_værdi"),  
  MEDICAL_DEVICE_CODE("medicalDeviceCode"),  
  MEDICAL_DEVICE_DISPLAYNAME("medicalDeviceDisplayName"),  
  MANUFACTURER_MODEL_NAME("manufacturerModelName"),  
  PHMR_MAALINGER_PATIENT("phmr_maalinger_patient");
 
  
  
  
  
  
  
 
  private String colmnName;

  CsvColumnNames(String colmnName) {
      this.colmnName = colmnName;
  }

  public String getColumnName() {
      return colmnName;
  }
  
  
  
}

