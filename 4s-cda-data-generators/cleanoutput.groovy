import groovy.io.FileType;

File file = new File("./target/datacreation/output");

file.traverse(type: FileType.FILES, nameFilter: ~/.*(\.csv|\.xml)/) {
  println "Deleting ${it.getName()}"
  it.delete();
}